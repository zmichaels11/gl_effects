#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include <cstddef>

#include <algorithm>
#include <array>
#include <iostream>
#include <random>
#include <sstream>
#include <utility>
#include <vector>

#include "share/gl_util.hpp"
#include "share/glfw_util.hpp"
#include "share/ro_file.hpp"

struct __attribute__ ((packed)) Point {
    float x, y;

    Point& operator+= (const Point& other) noexcept {
        x += other.x;
        y += other.y;

        return *this;
    }
};

struct __attribute__ ((packed)) Frame {
    float index;
    std::uint16_t u, v;
};

struct __attribute__ ((packed)) Polygon {
    Point upperLeft;
    Point upperRight;
    Point lowerLeft;
};

struct __attribute__ ((packed)) Sprite {
    Polygon shape;
    Frame frame;
    std::int32_t colorIndex;
};

struct __attribute__ ((packed)) Color {
    float r, g, b, a;
};

constexpr auto DEFAULT_WINDOW_WIDTH = std::size_t(640);
constexpr auto DEFAULT_WINDOW_HEIGHT = std::size_t(480);
constexpr auto MAX_COLORS = std::size_t(32);
constexpr auto MAX_SPRITES = std::size_t(1024);
constexpr auto FRAMES = std::size_t(10);
constexpr auto VBUFFER_SIZE = MAX_SPRITES * sizeof(Sprite);
constexpr auto TBUFFER_SIZE = MAX_COLORS * 4 * sizeof(float) * 4;
constexpr auto SPRITE_WIDTH = float(32.0F);
constexpr auto SPRITE_HEIGHT = float(32.0F);
constexpr auto MAX_HUE = float(360.0F);

struct SpriteData {
    Point pos;
    Point vel;
    int frame;
    std::int32_t colorIndex;
} sprites[MAX_SPRITES];

struct ColorData {
    float hue;
    float saturation;
    float value;    
} colors[MAX_COLORS];

void hsv(const ColorData& color, Color * ct0, Color * ct1, Color * ct2) {
    constexpr auto TO_RAD = 3.14159265358F / 180.0F;

    auto h = color.hue;  
    auto s = color.saturation;
    auto v = color.value;
    auto u = std::cos(h * TO_RAD);
    auto w = std::sin(h * TO_RAD);
    auto vsu = v * s * u;
    auto vsw = v * s * w;

    ct0[0] = {0.299F * v + 0.701F * vsu + 0.168F * vsw, 0.587F * v - 0.587F * vsu + 0.330F * vsw, 0.114F * v - 0.114F * vsu - 0.497F * vsw, 0.0F};
    ct1[0] = {0.299F * v - 0.299F * vsu - 0.328F * vsw, 0.587F * v + 0.413F * vsu + 0.035F * vsw, 0.114F * v - 0.114F * vsu + 0.292F * vsw, 0.0F};
    ct2[0] = {0.299F * v - 0.300F * vsu + 1.250F * vsw, 0.587F * v - 0.588F * vsu - 1.050F * vsw, 0.114F * v + 0.886F * vsu - 0.203F * vsw, 0.0F};
}

Frame frames[FRAMES];

void quad(Polygon& out, float x, float y, float w, float h) noexcept {
    out.upperLeft = {x, y};
    out.upperRight = {x + w, y};
    out.lowerLeft = {x, y + h};
}

void keyCallback(GLFWwindow * window, int key, int scancode, int action, int mods) {
    if (action == GLFW_PRESS) {
        switch (key) {
            case GLFW_KEY_ESCAPE:
                glfwSetWindowShouldClose(window, true);
                break;
            default:
                // unimplemented key code
                break;
        }
    }
}

int main(int argc, char ** argv) {
    auto window = share::glfw_util::initGraphics(DEFAULT_WINDOW_WIDTH, DEFAULT_WINDOW_HEIGHT, "Sprites");

    glfwSetKeyCallback(window, keyCallback);

    std::cout << "OpenGL Version: " << glGetString(GL_VERSION)
        << "\nOpenGL Renderer: " << glGetString(GL_RENDERER)
        << "\nOpenGL Shading Language Version: " << glGetString(GL_SHADING_LANGUAGE_VERSION)
        << std::endl;

    auto program = GLuint(0);
    auto vao = GLuint(0);
    auto spritesheet = GLuint(0);
    auto colorTransform = std::array<GLuint, 4>();
    auto vBuffer = GLuint(0);
    auto uBuffer = GLuint(0);
    auto tBuffer = GLuint(0);
    auto pSprites = static_cast<Sprite * > (nullptr);
    auto pColorTransforms = static_cast<Color * > (nullptr);

    {
        auto vsh = share::gl_util::newShader(GL_VERTEX_SHADER, "data/shaders/sprites.vert");
        auto gsh = share::gl_util::newShader(GL_GEOMETRY_SHADER, "data/shaders/sprites.geom");
        auto fsh = share::gl_util::newShader(GL_FRAGMENT_SHADER, "data/shaders/sprites.frag");
        auto shaders = std::vector<GLuint>();

        shaders.push_back(vsh);
        shaders.push_back(gsh);
        shaders.push_back(fsh);

        program = share::gl_util::newProgram(shaders);

        glDeleteShader(fsh);
        glDeleteShader(gsh);
        glDeleteShader(vsh);        

        glCreateVertexArrays(1, &vao);
        
        glEnableVertexArrayAttrib(vao, 0);
        glVertexArrayAttribFormat(vao, 0, 2, GL_FLOAT, false, 0);
        glVertexArrayAttribBinding(vao, 0, 0);

        glEnableVertexArrayAttrib(vao, 1);
        glVertexArrayAttribFormat(vao, 1, 2, GL_FLOAT, false, 8);
        glVertexArrayAttribBinding(vao, 1, 0);

        glEnableVertexArrayAttrib(vao, 2);
        glVertexArrayAttribFormat(vao, 2, 2, GL_FLOAT, false, 16);
        glVertexArrayAttribBinding(vao, 2, 0);

        glEnableVertexArrayAttrib(vao, 3);
        glVertexArrayAttribFormat(vao, 3, 1, GL_FLOAT, false, 24);
        glVertexArrayAttribBinding(vao, 3, 0);

        glEnableVertexArrayAttrib(vao, 4);
        glVertexArrayAttribFormat(vao, 4, 2, GL_UNSIGNED_SHORT, true, 28);
        glVertexArrayAttribBinding(vao, 4, 0);

        glEnableVertexArrayAttrib(vao, 5);
        glVertexArrayAttribIFormat(vao, 5, 1, GL_INT, 32);
        glVertexArrayAttribBinding(vao, 5, 0);
    }

    share::gl_util::checkError();

    {
        constexpr auto PERSISTENT_STORAGE = GL_MAP_WRITE_BIT | GL_MAP_PERSISTENT_BIT | GL_MAP_COHERENT_BIT;
        constexpr auto PERSISTENT_MAPPING = GL_MAP_WRITE_BIT | GL_MAP_PERSISTENT_BIT | GL_MAP_COHERENT_BIT;

        {
            glCreateBuffers(1, &vBuffer);
            glNamedBufferStorage(vBuffer, VBUFFER_SIZE, nullptr, PERSISTENT_STORAGE);

            auto mapping = glMapNamedBufferRange(vBuffer, 0, VBUFFER_SIZE, PERSISTENT_MAPPING);
            
            pSprites = reinterpret_cast<Sprite * > (mapping);
        }

        {
            glCreateBuffers(1, &tBuffer);
            glNamedBufferStorage(tBuffer, TBUFFER_SIZE, nullptr, PERSISTENT_STORAGE);

            auto mapping = glMapNamedBufferRange(tBuffer, 0, TBUFFER_SIZE, PERSISTENT_MAPPING);

            pColorTransforms = reinterpret_cast<Color * > (mapping);
        }
    }            
    
    {
        auto mProj = glm::ortho(
            0.0F, static_cast<float> (DEFAULT_WINDOW_WIDTH),
            static_cast<float> (DEFAULT_WINDOW_HEIGHT), 0.0F,
            0.0F, 1.0F);

        glCreateBuffers(1, &uBuffer);
        glNamedBufferStorage(uBuffer, 16 * sizeof(float), glm::value_ptr(mProj), 0);
    }

    share::gl_util::checkError();

    {
        using image_file = share::ro_file<share::image>;

        auto images = std::vector<image_file>();
        auto imageFrames = std::vector<const share::image * > ();

        auto maxWidth = std::size_t(0);
        auto maxHeight = std::size_t(0);

        for (std::size_t i = 0; i < FRAMES; i++) {
            auto filename = std::stringstream();

            filename << "data/images/duke/duke" << i << ".png";

            auto file = image_file(filename.str().c_str());
            auto const& data = file.data();

            maxWidth = std::max(maxWidth, data.width());
            maxHeight = std::max(maxHeight, data.height());
            
            images.push_back(std::move(file)); 
        }

        for (std::size_t i = 0; i < FRAMES; i++) {
            auto& image = images[i];
            auto& frame = image.data();

            imageFrames.push_back(&frame);

            frames[i].index = static_cast<float> (i);
            frames[i].u = share::gl_util::unormUI16(static_cast<float> (frame.width()) / static_cast<float> (maxWidth));
            frames[i].v = share::gl_util::unormUI16(static_cast<float> (frame.height()) / static_cast<float> (maxHeight));
        }

        spritesheet = share::gl_util::newTexture2DArray(imageFrames);
    }

    share::gl_util::checkError();

    {
        glCreateTextures(GL_TEXTURE_BUFFER, 4, colorTransform.data());

        constexpr auto CT_COMP_SIZE = MAX_COLORS * 4 * sizeof(float);
        auto offset = GLsizei(0);

        for (int i = 0; i < 4; i++) {
            glTextureBufferRange(colorTransform[i], GL_RGBA32F, tBuffer, offset, CT_COMP_SIZE);
            offset += CT_COMP_SIZE;
        }
    }

    share::gl_util::checkError();

    auto rng = std::default_random_engine();
    auto distX = std::uniform_real_distribution<float> (0, static_cast<float> (DEFAULT_WINDOW_WIDTH));
    auto distY = std::uniform_real_distribution<float> (0, static_cast<float> (DEFAULT_WINDOW_HEIGHT));
    auto distVel = std::uniform_real_distribution<float> (-1.0F, 1.0F);
    auto distFrame = std::uniform_int_distribution<int> (0, FRAMES - 1);
    auto distCT = std::uniform_int_distribution<int> (0, MAX_COLORS - 1);
    auto distHue = std::uniform_real_distribution<float> (0.0F, MAX_HUE);

    for (std::size_t i = 0; i < MAX_SPRITES; i++) {
        sprites[i].pos = { distX(rng), distY(rng) };
        sprites[i].vel = { distVel(rng), distVel(rng) };
        sprites[i].frame = distFrame(rng);
        sprites[i].colorIndex = distCT(rng);
    }

    for (std::size_t i = 0; i < MAX_COLORS; i++) {
        colors[i] = {distHue(rng), 1.0F, 1.0F};
    }

    glEnable(GL_BLEND);
    glBlendEquationSeparate(GL_FUNC_ADD, GL_FUNC_ADD);
    glBlendFuncSeparate(GL_ONE, GL_ONE_MINUS_SRC_ALPHA, GL_ONE, GL_ONE_MINUS_SRC_ALPHA);

    while (!glfwWindowShouldClose(window)) {
        {
            auto frameWidth = int(1);
            auto frameHeight = int(1);

            glfwGetFramebufferSize(window, &frameWidth, &frameHeight);

            glViewport(0, 0, frameWidth, frameHeight);
        }

        glClear(GL_COLOR_BUFFER_BIT);        

        for (std::size_t i = 0; i < MAX_SPRITES; i++) {
            quad(pSprites[i].shape, sprites[i].pos.x, sprites[i].pos.y, SPRITE_WIDTH, SPRITE_HEIGHT);
            pSprites[i].frame = frames[sprites[i].frame];
            pSprites[i].colorIndex = sprites[i].colorIndex;
            
            sprites[i].pos += sprites[i].vel;
            sprites[i].frame = (sprites[i].frame + 1) % FRAMES;

            constexpr auto MAX_SPRITE_X = static_cast<float> (DEFAULT_WINDOW_WIDTH) + SPRITE_WIDTH;
            constexpr auto MIN_SPRITE_X = -SPRITE_WIDTH;
            constexpr auto MAX_SPRITE_Y = static_cast<float> (DEFAULT_WINDOW_HEIGHT) + SPRITE_HEIGHT;
            constexpr auto MIN_SPRITE_Y = -SPRITE_HEIGHT;            

            if (sprites[i].pos.x > MAX_SPRITE_X || sprites[i].pos.x < MIN_SPRITE_X
                || sprites[i].pos.y > MAX_SPRITE_Y || sprites[i].pos.y < MIN_SPRITE_Y) {

                sprites[i].pos = { distX(rng), distY(rng) };
            }     
        }

        for (std::size_t i = 0; i < MAX_COLORS; i++) {
            auto hsv0 = pColorTransforms + i;
            auto hsv1 = hsv0 + MAX_COLORS;
            auto hsv2 = hsv1 + MAX_COLORS;
            auto scale = hsv2 + MAX_COLORS;

            colors[i].hue += 0.1F;

            if (colors[i].hue > MAX_HUE) {
                colors[i].hue -= MAX_HUE;
            }

            hsv(colors[i], hsv0, hsv1, hsv2);
            *scale = {1.0F, 1.0F, 1.0F, 1.0F};
        }

        glUseProgram(program);
        glBindBufferBase(GL_UNIFORM_BUFFER, 0, uBuffer);
        glBindTextureUnit(0, colorTransform[0]);
        glBindTextureUnit(1, colorTransform[1]);
        glBindTextureUnit(2, colorTransform[2]);
        glBindTextureUnit(3, colorTransform[3]);
        glBindTextureUnit(4, spritesheet);                

        glBindVertexArray(vao);
        glVertexArrayVertexBuffer(vao, 0, vBuffer, 0, sizeof(Sprite));

        glDrawArrays(GL_POINTS, 0, MAX_SPRITES);

        share::gl_util::checkError();

        glfwSwapBuffers(window);
        glfwPollEvents();
    }

    glUnmapNamedBuffer(tBuffer);
    glUnmapNamedBuffer(vBuffer);    

    glDeleteTextures(4, colorTransform.data());
    glDeleteTextures(1, &spritesheet);
    glDeleteBuffers(1, &uBuffer);
    glDeleteBuffers(1, &tBuffer);
    glDeleteBuffers(1, &vBuffer);
    glDeleteVertexArrays(1, &vao);
    glDeleteProgram(program);

    glfwDestroyWindow(window);

    glfwTerminate();
    return 0;
}