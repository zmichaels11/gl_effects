#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include <glm/mat4x4.hpp>
#include <glm/vec4.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include <cstring>

#include <iostream>
#include <random>

#include "share/gl_util.hpp"
#include "share/glfw_util.hpp"
#include "share/ro_file.hpp"

constexpr auto DEFAULT_WINDOW_WIDTH = std::size_t(640);
constexpr auto DEFAULT_WINDOW_HEIGHT = std::size_t(480);
constexpr auto POINTCLOUD_WIDTH = std::size_t(16);
constexpr auto POINTCLOUD_HEIGHT = std::size_t(16);
constexpr auto POINTCLOUD_DEPTH = std::size_t(16);
constexpr auto MAX_VOXELS = POINTCLOUD_WIDTH * POINTCLOUD_HEIGHT * POINTCLOUD_DEPTH;
constexpr auto DEFAULT_VOXEL_WIDTH = float(1.0F);
constexpr auto DEFAULT_VOXEL_HEIGHT = float(1.0F);
constexpr auto DEFAULT_VOXEL_DEPTH = float(1.0F);

struct CameraDataT {
    struct StepT {
        float x, y, z;
    } step;

    struct RotateT {
        float x, y, z;
    } rotate;
} camera;

struct __attribute__ ((packed)) VoxelLayerData {
    struct __attribute__ ((packed)) VoxelSize {
        float width, height, depth;
    } voxelSize;

    float padding;

    float projection[16];
};

void keyCallback(GLFWwindow * window, int key, int scancode, int action, int mods) {
    if (action == GLFW_PRESS) {
        switch (key) {
            case GLFW_KEY_ESCAPE:
                glfwSetWindowShouldClose(window, true);
                break;
            case GLFW_KEY_W:
                camera.step.z += 0.1F;
                break;
            case GLFW_KEY_S:    
                camera.step.z -= 0.1F;
                break;
            case GLFW_KEY_A:
                camera.step.x -= 0.1F;
                break;
            case GLFW_KEY_D:
                camera.step.x += 0.1F;
                break;
            case GLFW_KEY_I:
                camera.rotate.y += 0.1F;
                break;
            case GLFW_KEY_K:
                camera.rotate.y -= 0.1F;
                break;
            case GLFW_KEY_J:
                camera.rotate.x -= 0.1F;
                break;
            case GLFW_KEY_L:
                camera.rotate.x += 0.1F;
                break;
            default:
                // unimplemented key code
                break;
        }
    }
}

int main(int argc, char ** argv) {
    auto window = share::glfw_util::initGraphics(DEFAULT_WINDOW_WIDTH, DEFAULT_WINDOW_HEIGHT, "Voxels");

    glfwSetKeyCallback(window, keyCallback);

    std::cout << "OpenGL Version: " << glGetString(GL_VERSION)
        << "\nOpenGL Renderer: " << glGetString(GL_RENDERER)
        << "\nOpenGL Shading Language Version: " << glGetString(GL_SHADING_LANGUAGE_VERSION)
        << std::endl;
    
    auto program = GLuint(0);
    auto uBuffer = GLuint(0);
    auto vao = GLuint(0);
    auto pointCloud = GLuint(0);
    auto tileSheet = GLuint(0);
    auto vld = static_cast<VoxelLayerData * > (nullptr);

    {
        auto vsh = share::gl_util::newShader(GL_VERTEX_SHADER, "data/shaders/voxels.vert");
        auto gsh = share::gl_util::newShader(GL_GEOMETRY_SHADER, "data/shaders/voxels.geom");
        auto fsh = share::gl_util::newShader(GL_FRAGMENT_SHADER, "data/shaders/voxels.frag");
        auto shaders = std::vector<GLuint>();

        shaders.push_back(vsh);
        shaders.push_back(gsh);
        shaders.push_back(fsh);

        program = share::gl_util::newProgram(shaders);
        
        glDeleteShader(vsh);
        glDeleteShader(gsh);
        glDeleteShader(fsh);        

        glCreateVertexArrays(1, &vao);        
    }

    {
        glCreateBuffers(1, &uBuffer);
        glNamedBufferStorage(uBuffer, sizeof(VoxelLayerData), nullptr, GL_MAP_WRITE_BIT | GL_MAP_PERSISTENT_BIT | GL_MAP_COHERENT_BIT);

        auto mapping = glMapNamedBufferRange(uBuffer, 0, sizeof(VoxelLayerData), GL_MAP_WRITE_BIT | GL_MAP_PERSISTENT_BIT | GL_MAP_COHERENT_BIT);

        vld = reinterpret_cast<VoxelLayerData * > (mapping);
        vld->voxelSize.width = DEFAULT_VOXEL_WIDTH;
        vld->voxelSize.height = DEFAULT_VOXEL_HEIGHT;
        vld->voxelSize.depth = DEFAULT_VOXEL_DEPTH;
    }

    {
        auto tile0File = share::ro_file<share::image> ("data/images/tiles/castle0.png");
        auto tile1File = share::ro_file<share::image> ("data/images/tiles/dirt0.png");

        auto pTiles = std::vector<const share::image * > ();

        pTiles.push_back(nullptr);
        pTiles.push_back(&tile0File.data());
        pTiles.push_back(&tile1File.data());

        tileSheet = share::gl_util::newTexture2DArray(pTiles);

        glCreateTextures(GL_TEXTURE_3D, 1, &pointCloud);
        glTextureStorage3D(pointCloud, 1, GL_R8, POINTCLOUD_WIDTH, POINTCLOUD_HEIGHT, POINTCLOUD_DEPTH);

        auto pointCloudData = std::make_unique<unsigned char[]> (MAX_VOXELS);

        auto rng = std::default_random_engine();
        auto dist = std::uniform_int_distribution<unsigned char> (0, 2);

        auto i = std::size_t(0);
        auto j = std::size_t(0);
        auto k = std::size_t(0);        

        for (k = 0; k < POINTCLOUD_DEPTH; k++) {
            for (j = 0; j < POINTCLOUD_HEIGHT; j++) {
                for (i = 0; i < POINTCLOUD_WIDTH; i++) {          
                    pointCloudData[k * POINTCLOUD_WIDTH * POINTCLOUD_HEIGHT + j * POINTCLOUD_WIDTH + i] = dist(rng);
                }
            }
        }

        glTextureSubImage3D(
            pointCloud, 
            0, 0, 0, 0,
            POINTCLOUD_WIDTH, POINTCLOUD_HEIGHT, POINTCLOUD_DEPTH, 
            GL_RED, GL_UNSIGNED_BYTE, 
            pointCloudData.get());
    }

    share::gl_util::checkError();

    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);
    glClearDepth(1.0);

    glEnable(GL_BLEND);
    glBlendEquationSeparate(GL_FUNC_ADD, GL_FUNC_ADD);
    glBlendFuncSeparate(GL_ONE, GL_ONE_MINUS_SRC_ALPHA, GL_ONE, GL_ONE_MINUS_SRC_ALPHA);

    camera.step.z = -6.0F;    

    while (!glfwWindowShouldClose(window)) {      
        {
            auto aspectRatio = float(1.0F);

            {
                auto frameWidth = int(0);
                auto frameHeight = int(0);

                glfwGetFramebufferSize(window, &frameWidth, &frameHeight);

                aspectRatio = static_cast<float> (frameWidth) / static_cast<float> (frameHeight);

                glViewport(0, 0, frameWidth, frameHeight);
            }

            constexpr auto FOV = float(90.0F);
            constexpr auto NEAR_CLIP = float(0.1F);
            constexpr auto FAR_CLIP = float(100.0F);

            auto fovRadians = FOV * glm::pi<float>() / 180.0F;

            auto mProj = glm::perspective(fovRadians, aspectRatio, NEAR_CLIP, FAR_CLIP);   
            auto mView = glm::translate(glm::mat4x4(1.0F), glm::vec3(camera.step.x, camera.step.y, camera.step.z));

            mView = glm::rotate(mView, camera.rotate.y, glm::vec3(-1.0F, 0.0F, 0.0F));
            mView = glm::rotate(mView, camera.rotate.x, glm::vec3(0.0F, 1.0F, 0.0F));

            auto mModel = glm::translate(glm::mat4(1.0F), glm::vec3(-8.0F, -8.0F, -24.0F));

            auto mvp = mProj * mView * mModel;
            
            std::memcpy(vld->projection, glm::value_ptr(mvp), sizeof(float) * 16);
        }

        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        glUseProgram(program);        
        glBindBufferBase(GL_UNIFORM_BUFFER, 0, uBuffer);        
        glBindTextureUnit(0, pointCloud);
        glBindTextureUnit(1, tileSheet);

        glBindVertexArray(vao);
        glDrawArrays(GL_POINTS, 0, MAX_VOXELS);

        share::gl_util::checkError();

        glfwSwapBuffers(window);
        glfwPollEvents();        
    }

    glUnmapNamedBuffer(uBuffer);

    glDeleteTextures(1, &tileSheet);
    glDeleteTextures(1, &pointCloud);
    glDeleteVertexArrays(1, &vao);
    glDeleteBuffers(1, &uBuffer);
    glDeleteProgram(program);

    glfwDestroyWindow(window);

    glfwTerminate();
    return 0;
}