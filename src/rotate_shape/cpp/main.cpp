#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include <cstddef>
#include <cstdint>
#include <cstring>

#include <iostream>
#include <vector>

#include "share/gl_util.hpp"
#include "share/glfw_util.hpp"
#include "share/ro_file.hpp"

struct __attribute__ ((packed)) Color {
    std::uint8_t r, g, b, a;
};

struct __attribute__ ((packed)) Point {
    float x, y, z;
};

struct __attribute__ ((packed)) Vertex {
    Point position;
    Color color;
};

constexpr auto DEFAULT_WINDOW_WIDTH = std::size_t(640);
constexpr auto DEFAULT_WINDOW_HEIGHT = std::size_t(480);
constexpr auto MAX_SHAPES = std::size_t(2);

void keyCallback(GLFWwindow * window, int key, int scancode, int action, int mods) {
    if (action == GLFW_PRESS) {
        switch (key) {
            case GLFW_KEY_ESCAPE:
                glfwSetWindowShouldClose(window, true);
                break;
            default:
                // unimplemented key code
                break;
        }
    }
}

int main(int argc, char ** argv) {
    auto window = share::glfw_util::initGraphics(DEFAULT_WINDOW_WIDTH, DEFAULT_WINDOW_HEIGHT, "Rotate Shape");

    glfwSetKeyCallback(window, keyCallback);

    std::cout << "OpenGL Version: " << glGetString(GL_VERSION)
        << "\nOpenGL Renderer: " << glGetString(GL_RENDERER)
        << "\nOpenGL Shading Language Version: " << glGetString(GL_SHADING_LANGUAGE_VERSION)
        << std::endl;

    auto program = GLuint(0);
    auto vBuffer = GLuint(0);
    auto uBuffer = GLuint(0);
    auto vao = GLuint(0);
    auto pMatrices = static_cast<float * > (nullptr);

    {
        auto vsh = share::gl_util::newShader(GL_VERTEX_SHADER, "data/shaders/primitive_mvp.vert");
        auto fsh = share::gl_util::newShader(GL_FRAGMENT_SHADER, "data/shaders/primitive.frag");
        auto shaders = std::vector<GLuint>();

        shaders.push_back(vsh);
        shaders.push_back(fsh);

        program = share::gl_util::newProgram(shaders);

        glDeleteShader(fsh);
        glDeleteShader(vsh);

        glCreateVertexArrays(1, &vao);
        glEnableVertexArrayAttrib(vao, 0);
        glVertexArrayAttribFormat(vao, 0, 3, GL_FLOAT, false, 0);
        glVertexArrayAttribBinding(vao, 0, 0);

        glEnableVertexArrayAttrib(vao, 1);
        glVertexArrayAttribFormat(vao, 1, 4, GL_UNSIGNED_BYTE, true, 12);
        glVertexArrayAttribBinding(vao, 1, 0);        
    }    

    {
        auto vertices = std::vector<Vertex>();

        vertices.push_back({{0.0F, 1.0F, 0.0F}, {0xFF, 0x00, 0x00, 0xFF}});
        vertices.push_back({{-1.0F, -1.0F, 0.0F}, {0x00, 0xFF, 0x00, 0xFF}});
        vertices.push_back({{1.0F, -1.0F, 0.0F}, {0x00, 0x00, 0xFF, 0xFF}});

        vertices.push_back({{-1.0F, 1.0F, 0.0F}, {0xFF, 0x00, 0x00, 0xFF}});
        vertices.push_back({{-1.0F, -1.0F, 0.0F}, {0x00, 0xFF, 0x00, 0xFF}});
        vertices.push_back({{1.0F, 1.0F, 0.0F}, {0x00, 0x00, 0xFF, 0xFF}});
        vertices.push_back({{1.0F, -1.0F, 0.0F}, {0xFF, 0xFF, 0x00, 0xFF}});

        auto nVertices = vertices.size();

        glCreateBuffers(1, &vBuffer);
        glNamedBufferStorage(vBuffer, nVertices * sizeof(Vertex), vertices.data(), 0);                
    }

    constexpr auto MATRIX_SIZE = sizeof(float) * 16;
    auto matrixSizeAligned = MATRIX_SIZE;

    {        
        auto offsetAlignment = GLint(0);

        glGetIntegerv(GL_UNIFORM_BUFFER_OFFSET_ALIGNMENT, &offsetAlignment);

        matrixSizeAligned = share::gl_util::alignUp(MATRIX_SIZE, offsetAlignment);

        constexpr auto PERSISTENT_STORAGE = GL_MAP_WRITE_BIT | GL_MAP_PERSISTENT_BIT | GL_MAP_COHERENT_BIT;
        constexpr auto PERSISTENT_MAPPING = GL_MAP_WRITE_BIT | GL_MAP_PERSISTENT_BIT | GL_MAP_COHERENT_BIT;

        auto uBufferSize = matrixSizeAligned * 2;

        glCreateBuffers(1, &uBuffer);
        glNamedBufferStorage(uBuffer, uBufferSize, nullptr, PERSISTENT_STORAGE);

        auto mapping = glMapNamedBufferRange(uBuffer, 0, uBufferSize, PERSISTENT_MAPPING);

        pMatrices = reinterpret_cast<float * > (mapping);
    }

    while (!glfwWindowShouldClose(window)) {
        {
            static auto rotTri = float(0.0F);
            static auto rotQuad = float(0.0F);

            auto aspectRatio = float(1.0F);

            {
                auto frameWidth = int(0);
                auto frameHeight = int(0);

                glfwGetFramebufferSize(window, &frameWidth, &frameHeight);

                aspectRatio = static_cast<float> (frameWidth) / static_cast<float> (frameHeight);

                glViewport(0, 0, frameWidth, frameHeight);
            }
            
            auto projection = glm::perspective(glm::pi<float>() * 0.5F, aspectRatio, 0.1F, 100.0F);

            auto mv0 = glm::translate(glm::mat4x4(1.0F), glm::vec3(-1.5F, 0.0F, -6.0F));
            mv0 = glm::rotate(mv0, rotTri, glm::vec3(0.0F, 1.0F, 0.0F));

            auto mv1 = glm::translate(glm::mat4x4(1.0F), glm::vec3(1.5F, 0.0F, -6.0F));
            mv1 = glm::rotate(mv1, rotQuad, glm::vec3(1.0F, 0.0F, 0.0F));            

            auto it = pMatrices;

            std::memcpy(it, glm::value_ptr(projection), MATRIX_SIZE);
            std::memcpy(it + 16, glm::value_ptr(mv0), MATRIX_SIZE);

            it += (matrixSizeAligned / sizeof(float));
            std::memcpy(it, glm::value_ptr(projection), MATRIX_SIZE);
            std::memcpy(it + 16, glm::value_ptr(mv1), MATRIX_SIZE);

            rotTri += 0.2F;
            rotQuad -= 0.15F;
        }

        glClear(GL_COLOR_BUFFER_BIT);        

        glUseProgram(program);        

        glBindVertexArray(vao);
        glVertexArrayVertexBuffer(vao, 0, vBuffer, 0, sizeof(Vertex));

        glBindBufferRange(GL_UNIFORM_BUFFER, 0, uBuffer, 0, matrixSizeAligned);
        glDrawArrays(GL_TRIANGLE_STRIP, 0, 3);

        glBindBufferRange(GL_UNIFORM_BUFFER, 0, uBuffer, matrixSizeAligned, matrixSizeAligned);
        glDrawArrays(GL_TRIANGLE_STRIP, 3, 4);

        share::gl_util::checkError();

        glfwSwapBuffers(window);
        glfwPollEvents();
    }

    glUnmapNamedBuffer(uBuffer);

    glDeleteBuffers(1, &uBuffer);
    glDeleteBuffers(1, &vBuffer);
    glDeleteVertexArrays(1, &vao);
    glDeleteProgram(program);

    glfwDestroyWindow(window);

    glfwTerminate();
    return 0;
}