#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/mat4x4.hpp>
#include <glm/vec4.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include <cstddef>
#include <cstdint>
#include <cstring>

#include <iostream>
#include <vector>

#include "share/gl_util.hpp"
#include "share/glfw_util.hpp"
#include "share/ro_file.hpp"

struct __attribute__ ((packed)) Color {
    std::uint8_t r, g, b, a;
};

struct __attribute__ ((packed)) Point {
    float x, y, z;
};

struct __attribute__ ((packed)) Vertex {
    Point position;
    Color color;
};

void keyCallback(GLFWwindow * window, int key, int scancode, int action, int mods) {
    if (action == GLFW_PRESS) {
        switch (key) {
            case GLFW_KEY_ESCAPE:
                glfwSetWindowShouldClose(window, true);
                break;
            default:
                // unimplemented key code
                break;
        }
    }
}

constexpr auto DEFAULT_WINDOW_WIDTH = std::size_t(640);
constexpr auto DEFAULT_WINDOW_HEIGHT = std::size_t(480);

int main(int argc, char ** argv) {
    auto window = share::glfw_util::initGraphics(DEFAULT_WINDOW_WIDTH, DEFAULT_WINDOW_HEIGHT, "Simple Shape");

    glfwSetKeyCallback(window, keyCallback);

    std::cout << "OpenGL Version: " << glGetString(GL_VERSION)
        << "\nOpenGL Renderer: " << glGetString(GL_RENDERER)
        << "\nOpenGL Shading Language Version: " << glGetString(GL_SHADING_LANGUAGE_VERSION)
        << std::endl;

    auto program = GLuint(0);    
    auto vBuffer = GLuint(0);
    auto uBuffer = GLuint(0);
    auto vao = GLuint(0);
    auto nVertices = std::size_t(0);
    auto projection = static_cast<void * > (nullptr);    

    {
        auto vsh = share::gl_util::newShader(GL_VERTEX_SHADER, "data/shaders/primitive.vert");
        auto fsh = share::gl_util::newShader(GL_FRAGMENT_SHADER, "data/shaders/primitive.frag");
        auto shaders = std::vector<GLuint>();

        shaders.push_back(vsh);
        shaders.push_back(fsh);

        program = share::gl_util::newProgram(shaders);

        glDeleteShader(fsh);
        glDeleteShader(vsh);

        glCreateVertexArrays(1, &vao);
        glEnableVertexArrayAttrib(vao, 0);
        glVertexArrayAttribFormat(vao, 0, 3, GL_FLOAT, false, 0);
        glVertexArrayAttribBinding(vao, 0, 0);

        glEnableVertexArrayAttrib(vao, 1);
        glVertexArrayAttribFormat(vao, 1, 4, GL_UNSIGNED_BYTE, true, 12);
        glVertexArrayAttribBinding(vao, 1, 0);        
    }

    {
        auto vertices = std::vector<Vertex>();

        vertices.push_back({{-1.5F, 1.0F, 0.0F}, {0xFF, 0x00, 0x00, 0xFF}});
        vertices.push_back({{-2.5F, -1.0F, 0.0F}, {0x00, 0xFF, 0x00, 0xFF}});
        vertices.push_back({{-0.5F, -1.0F, 0.0F}, {0x00, 0x00, 0xFF, 0xFF}});    vertices.push_back(vertices.back());

        vertices.push_back({{0.5F, 1.0F, 0.0F}, {0xFF, 0x00, 0x00, 0xFF}});    vertices.push_back(vertices.back());
        vertices.push_back({{0.5F, -1.0F, 0.0F}, {0x00, 0xFF, 0x00, 0xFF}});
        vertices.push_back({{2.5F, 1.0F, 0.0F}, {0x00, 0x00, 0xFF, 0xFF}});
        vertices.push_back({{2.5F, -1.0F, 0.0F}, {0xFF, 0xFF, 0x00, 0xFF}});

        nVertices = vertices.size();

        glCreateBuffers(1, &vBuffer);
        glNamedBufferStorage(vBuffer, nVertices * sizeof(Vertex), vertices.data(), 0);
    }
           
    glCreateBuffers(1, &uBuffer);
    glNamedBufferStorage(uBuffer, 16 * sizeof(float), nullptr, GL_MAP_WRITE_BIT | GL_MAP_PERSISTENT_BIT | GL_MAP_COHERENT_BIT);

    projection = glMapNamedBufferRange(uBuffer, 0, sizeof(float) * 16, GL_MAP_WRITE_BIT | GL_MAP_PERSISTENT_BIT | GL_MAP_COHERENT_BIT);    

    while (!glfwWindowShouldClose(window)) {
        {
            constexpr auto FOV = float(90.0F);
            constexpr auto NEAR_CLIP = float(0.1F);
            constexpr auto FAR_CLIP = float(100.0F);

            auto aspectRatio = float(1.0F);

            {
                auto frameWidth = int(0);
                auto frameHeight = int(0);

                glfwGetFramebufferSize(window, &frameWidth, &frameHeight);

                aspectRatio = static_cast<float> (frameWidth) / static_cast<float> (frameHeight);

                glViewport(0, 0, frameWidth, frameHeight);
            }

            auto fovRadians = FOV * glm::pi<float>() / 180.0F;

            auto p = glm::perspective(fovRadians, aspectRatio, NEAR_CLIP, FAR_CLIP);
            auto m = glm::translate(glm::mat4(1.0F), glm::vec3(0.0F, 0.0F, -6.0F));
            auto pmv = p * m;

            std::memcpy(projection, glm::value_ptr(pmv), sizeof(float) * 16);
        }

        glClear(GL_COLOR_BUFFER_BIT);

        glUseProgram(program);
        glBindBufferBase(GL_UNIFORM_BUFFER, 0, uBuffer);

        glBindVertexArray(vao);
        glVertexArrayVertexBuffer(vao, 0, vBuffer, 0, sizeof(Vertex));

        glDrawArrays(GL_TRIANGLE_STRIP, 0, nVertices);

        share::gl_util::checkError();

        glfwSwapBuffers(window);
        glfwPollEvents();
    }

    glUnmapNamedBuffer(uBuffer);

    glDeleteBuffers(1, &uBuffer);
    glDeleteBuffers(1, &vBuffer);
    glDeleteVertexArrays(1, &vao);
    glDeleteProgram(program);

    glfwDestroyWindow(window);

    glfwTerminate();
    return 0;
}