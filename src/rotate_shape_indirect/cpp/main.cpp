#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include <cstddef>
#include <cstdint>
#include <cstring>

#include <iostream>
#include <vector>

#include "share/gl_util.hpp"
#include "share/glfw_util.hpp"
#include "share/ro_file.hpp"

struct __attribute__ ((packed)) Color {
    std::uint8_t r, g, b, a;
};

struct __attribute__ ((packed)) Point {
    float x, y, z;
};

struct __attribute__ ((packed)) Vertex {
    Point position;
    Color color;
};

constexpr auto DEFAULT_WINDOW_WIDTH = std::size_t(640);
constexpr auto DEFAULT_WINDOW_HEIGHT = std::size_t(480);
constexpr auto MAX_SHAPES = std::size_t(2);
constexpr auto MATRIX_SIZE = sizeof(float) * 16;
constexpr auto PERSISTENT_STORAGE = GL_MAP_WRITE_BIT | GL_MAP_PERSISTENT_BIT | GL_MAP_COHERENT_BIT;
constexpr auto PERSISTENT_MAPPING = GL_MAP_WRITE_BIT | GL_MAP_PERSISTENT_BIT | GL_MAP_COHERENT_BIT;

void keyCallback(GLFWwindow * window, int key, int scancode, int action, int mods) {
    if (action == GLFW_PRESS) {
        switch (key) {
            case GLFW_KEY_ESCAPE:
                glfwSetWindowShouldClose(window, true);
                break;
            default:
                // unimplemented key code
                break;
        }
    }
}

int main(int argc, char ** argv) {
    auto window = share::glfw_util::initGraphics(DEFAULT_WINDOW_WIDTH, DEFAULT_WINDOW_HEIGHT, "Rotate Shape");

    glfwSetKeyCallback(window, keyCallback);

    std::cout << "OpenGL Version: " << glGetString(GL_VERSION)
        << "\nOpenGL Renderer: " << glGetString(GL_RENDERER)
        << "\nOpenGL Shading Language Version: " << glGetString(GL_SHADING_LANGUAGE_VERSION)
        << std::endl;    

    struct Programs {
        GLuint render;
        GLuint simulate;
    } programs;    

    {
        auto csh = share::gl_util::newShader(GL_COMPUTE_SHADER, "data/shaders/primitive_mvp_indirect.comp");
        auto shaders = std::vector<GLuint>();

        shaders.push_back(csh);

        programs.simulate = share::gl_util::newProgram(shaders);

        glDeleteShader(csh);
    }

    {
        auto vsh = share::gl_util::newShader(GL_VERTEX_SHADER, "data/shaders/primitive_mvp_instanced.vert");
        auto fsh = share::gl_util::newShader(GL_FRAGMENT_SHADER, "data/shaders/primitive.frag");
        auto shaders = std::vector<GLuint>();

        shaders.push_back(vsh);
        shaders.push_back(fsh);

        programs.render = share::gl_util::newProgram(shaders);

        glDeleteShader(fsh);
        glDeleteShader(vsh);
    }    

    auto vao = GLuint(0);

    glCreateVertexArrays(1, &vao);
    glEnableVertexArrayAttrib(vao, 0);
    glVertexArrayAttribFormat(vao, 0, 3, GL_FLOAT, false, 0);
    glVertexArrayAttribBinding(vao, 0, 0);

    glEnableVertexArrayAttrib(vao, 1);
    glVertexArrayAttribFormat(vao, 1, 4, GL_UNSIGNED_BYTE, true, 12);
    glVertexArrayAttribBinding(vao, 1, 0);

    glEnableVertexArrayAttrib(vao, 2);
    glVertexArrayAttribFormat(vao, 2, 4, GL_FLOAT, false, 0);
    glVertexArrayAttribBinding(vao, 2, 1);

    glEnableVertexArrayAttrib(vao, 3);
    glVertexArrayAttribFormat(vao, 3, 4, GL_FLOAT, false, 16);
    glVertexArrayAttribBinding(vao, 3, 1);

    glEnableVertexArrayAttrib(vao, 4);
    glVertexArrayAttribFormat(vao, 4, 4, GL_FLOAT, false, 32);
    glVertexArrayAttribBinding(vao, 4, 1);

    glEnableVertexArrayAttrib(vao, 5);
    glVertexArrayAttribFormat(vao, 5, 4, GL_FLOAT, false, 48);
    glVertexArrayAttribBinding(vao, 5, 1);

    glVertexArrayBindingDivisor(vao, 1, 1);

    struct Buffers {
        GLuint vertices;
        GLuint matrices;
        GLuint projection;
        GLuint parameters;
        GLuint command;
    } buffers;    

    {
        auto vertices = std::vector<Vertex>();

        vertices.push_back({{0.0F, 1.0F, 0.0F}, {0xFF, 0x00, 0x00, 0xFF}});
        vertices.push_back({{-1.0F, -1.0F, 0.0F}, {0x00, 0xFF, 0x00, 0xFF}});
        vertices.push_back({{1.0F, -1.0F, 0.0F}, {0x00, 0x00, 0xFF, 0xFF}});

        vertices.push_back({{-1.0F, 1.0F, 0.0F}, {0xFF, 0x00, 0x00, 0xFF}});
        vertices.push_back({{-1.0F, -1.0F, 0.0F}, {0x00, 0xFF, 0x00, 0xFF}});
        vertices.push_back({{1.0F, 1.0F, 0.0F}, {0x00, 0x00, 0xFF, 0xFF}});
        vertices.push_back({{1.0F, -1.0F, 0.0F}, {0xFF, 0xFF, 0x00, 0xFF}});

        auto nVertices = vertices.size();        

        glCreateBuffers(1, &buffers.vertices);
        glNamedBufferStorage(buffers.vertices, nVertices * sizeof(Vertex), vertices.data(), 0);                
    }

    {
        constexpr auto MATRIX_BUFFER_SIZE = MAX_SHAPES * MATRIX_SIZE;

        auto initData = std::make_unique<glm::mat4x4[]> (MAX_SHAPES);

        for (std::size_t i = 0; i < MAX_SHAPES; i++) {
            initData[i] = glm::mat4x4(
                1.0F, 0.0F, 0.0F, 0.0F,
                0.0F, 1.0F, 0.0F, 0.0F,
                0.0F, 0.0F, 1.0F, 0.0F,
                0.0F, 0.0F, -6.0F, 1.0F);
        }

        glCreateBuffers(1, &buffers.matrices);
        glNamedBufferStorage(buffers.matrices, MATRIX_BUFFER_SIZE, initData.get(), 0);
    }

    auto pProjection = static_cast<glm::mat4x4 * > (nullptr);

    {        
        glCreateBuffers(1, &buffers.projection);
        glNamedBufferStorage(buffers.projection, MATRIX_SIZE, nullptr, PERSISTENT_STORAGE);

        auto mapping = glMapNamedBufferRange(buffers.projection, 0, MATRIX_SIZE, PERSISTENT_MAPPING);

        pProjection = reinterpret_cast<glm::mat4x4 * > (mapping);
    }

    {
        struct __attribute__ ((packed)) Parameters {
            struct __attribute__ ((packed)) Rotation {
                float x, y, z, a;
            } rotation;

            struct __attribute__ ((packed)) Increment {
                float value;
                float padding[3];
            } increment;

            struct __attribute__ ((packed)) Translation {
                float x, y, z;
                float padding;
            } translation;
        };

        auto temp = std::make_unique<Parameters[]> (MAX_SHAPES);

        temp[0].rotation.y = 1.0F;
        temp[0].increment.value = 0.2F;
        temp[0].translation.x = -1.5F;
        temp[0].translation.z = -6.0F;

        temp[1].rotation.x = 1.0F;
        temp[1].increment.value = 0.15F;
        temp[1].translation.x = 1.5F;
        temp[1].translation.z = -6.0F;

        glCreateBuffers(1, &buffers.parameters);
        glNamedBufferStorage(buffers.parameters, MAX_SHAPES * sizeof(Parameters), temp.get(), 0);
    }

    {
        struct __attribute__ ((packed)) Command {
            GLuint count;
            GLuint instanceCount;
            GLuint first;
            GLuint baseInstance;
        };

        auto cmds = std::vector<Command>();

        cmds.push_back({3, 1, 0, 0});
        cmds.push_back({4, 1, 3, 1});

        glCreateBuffers(1, &buffers.command);
        glNamedBufferStorage(buffers.command, sizeof(Command) * cmds.size(), cmds.data(), 0);
    }

    while (!glfwWindowShouldClose(window)) {
        {
            auto aspectRatio = float(1.0F);

            {
                auto frameWidth = int(0);
                auto frameHeight = int(0);

                glfwGetFramebufferSize(window, &frameWidth, &frameHeight);

                aspectRatio = static_cast<float> (frameWidth) / static_cast<float> (frameHeight);

                glViewport(0, 0, frameWidth, frameHeight);
            }
                        
            *pProjection = glm::perspective(glm::pi<float>() * 0.5F, aspectRatio, 0.1F, 100.0F);
        }

        glUseProgram(programs.simulate);
        glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 0, buffers.parameters);
        glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 1, buffers.matrices);
        glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);
        glDispatchCompute(MAX_SHAPES, 1, 1);
        glMemoryBarrier(GL_VERTEX_ATTRIB_ARRAY_BARRIER_BIT);

        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);        

        glUseProgram(programs.render);
        glBindBufferBase(GL_UNIFORM_BUFFER, 0, buffers.projection);
        
        glBindVertexArray(vao);
        glVertexArrayVertexBuffer(vao, 0, buffers.vertices, 0, sizeof(Vertex));
        glVertexArrayVertexBuffer(vao, 1, buffers.matrices, 0, MATRIX_SIZE);

        glBindBuffer(GL_DRAW_INDIRECT_BUFFER, buffers.command);
        glMultiDrawArraysIndirect(GL_TRIANGLE_STRIP, 0, 2, 0);

        share::gl_util::checkError();

        glfwSwapBuffers(window);
        glfwPollEvents();
    }
    
    glUnmapNamedBuffer(buffers.projection);

    glDeleteBuffers(1, &buffers.command);
    glDeleteBuffers(1, &buffers.projection);
    glDeleteBuffers(1, &buffers.vertices);
    glDeleteBuffers(1, &buffers.parameters);
    glDeleteVertexArrays(1, &vao);
    glDeleteProgram(programs.simulate);
    glDeleteProgram(programs.render);

    glfwDestroyWindow(window);

    glfwTerminate();
    return 0;
}