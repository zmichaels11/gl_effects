#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include <cmath>
#include <cstdint>

#include <array>
#include <iostream>
#include <stdexcept>
#include <string>
#include <vector>

#include "share/gl_util.hpp"
#include "share/glfw_util.hpp"

#include "share/ro_file.hpp"

struct __attribute__ ((packed)) Color {
    std::uint8_t r, g, b, a;

    Color lerp(const Color& o, float ratio) const {        
        auto sr = static_cast<std::uint8_t> (r * ratio + (1.0F - ratio) * o.r);
        auto sg = static_cast<std::uint8_t> (g * ratio + (1.0F - ratio) * o.g);
        auto sb = static_cast<std::uint8_t> (b * ratio + (1.0F - ratio) * o.b);
        auto sa = static_cast<std::uint8_t> (a * ratio + (1.0F - ratio) * o.a);    

        return {sr, sg, sb, sa};        
    }
};

struct __attribute__ ((packed)) Point {
    float x, y;

    Point operator+ (const Point& o) const {
        return {x + o.x, y + o.y};
    }

    Point operator- (const Point& o) const {
        return {x - o.x, y - o.y};
    }

    auto dot(const Point& o) const -> decltype(x * x + y * y) {
        return x * o.x + y * o.y;
    }
};

struct __attribute__ ((packed)) GradientVertex {
    Point point;
    Color color;
};

struct __attribute__ ((packed)) ColorStop {
    float r;
    Color color;
};

void hardLinear(
    std::vector<GradientVertex>& gbuf,
    Point p, Point size,
    Point s, Point e,
    const std::vector<ColorStop>& stops) {

    constexpr auto SUBDIVS = std::size_t(8);

    auto a = Point{e.x - s.x, e.y - s.y};
    auto nlen2 = 1.0F / a.dot(a);

    auto find = [&] (auto idx) {
        auto left = static_cast<const ColorStop * > (nullptr);
        auto right = static_cast<const ColorStop * > (nullptr);

        if (idx >= 1.0F) {
            right = &stops.back();
            left = right - 1;
            idx = 1.0F;
        } else if (idx <= 0.0F) {
            left = &stops.front();
            right = left + 1;
            idx = 0.0F;
        } else {
            for (auto it = stops.rbegin(); it != stops.rend(); ++it) {
                if (it->r <= idx) {
                    left = &(*it);
                    break;
                }
            }

            for (auto it = stops.begin(); it != stops.end(); it++) {
                if (it->r > idx) {
                    right = &(*it);
                    break;
                }
            }

            if (left == nullptr) {
                left = &stops.front();
            }

            if (right == nullptr) {
                right = &stops.back();
            }
        }

        auto l = left->r;
        auto r = right->r;
        auto size = r - l;

        auto ratio = static_cast<float> (idx - left->r) / static_cast<float> (size);

        return left->color.lerp(right->color, 1.0F - ratio);
    };

    for (std::size_t j = 0; j < SUBDIVS; j++) {
        auto v0 = static_cast<float> (j) / SUBDIVS;
        auto v1 = static_cast<float> (j + 1) / SUBDIVS;

        auto y0 = p.y + v0 * size.y;
        auto y1 = p.y + v1 * size.y;

        for (std::size_t i = 0; i < SUBDIVS; i++) {
            auto u = static_cast<float> (i) / SUBDIVS;            

            auto x = p.x + u * size.x;

            auto b0 = Point{u - s.x, v0 - s.y};
            auto b1 = Point{u - s.x, v1 - s.y};

            auto idx0 = a.dot(b0) * nlen2;
            auto idx1 = a.dot(b1) * nlen2;

            auto c0 = find(idx0);
            auto c1 = find(idx1);            

            if (i == 0) {
                gbuf.push_back({{x, y0}, c0});    
            }

            gbuf.push_back({{x, y0}, c0});
            gbuf.push_back({{x, y1}, c1});

            if (i == SUBDIVS - 1) {                
                gbuf.push_back({{x, y1}, c1});
            }
        }
    }
}

void radial(
    std::vector<GradientVertex>& gbuf,    
    Point p, float r, 
    const std::vector<ColorStop>& stops) {
    
    constexpr auto ARC = 2.0F * 3.14159265358F;
    constexpr auto SLICES = std::size_t(12);

    auto rings = stops.size() - 1;

    for (decltype(rings) i = 0; i < rings; i++) {
        auto r0 = stops[i].r * r;
        auto r1 = stops[i + 1].r * r;
        auto& c0 = stops[i].color;
        auto& c1 = stops[i + 1].color;        
        auto step = ARC / SLICES;        

        for (std::size_t j = 0; j <= SLICES; j++) {            
            auto ca = std::cos(j * step);
            auto sa = std::sin(j * step);
            
            auto x0 = p.x + ca * r0;
            auto x1 = p.x + ca * r1;
            auto y0 = p.y + sa * r0;
            auto y1 = p.y + sa * r1;

            if (j == 0) {
                gbuf.push_back({{x0, y0}, c0});    
            }

            gbuf.push_back({{x0, y0}, c0});
            gbuf.push_back({{x1, y1}, c1});

            if (j == SLICES) {                
                gbuf.push_back({{x1, y1}, c1});
            }
        }
    }

    gbuf.push_back(gbuf.back());
}

constexpr auto DEFAULT_WINDOW_WIDTH = std::size_t(640);
constexpr auto DEFAULT_WINDOW_HEIGHT = std::size_t(480);

void keyCallback(GLFWwindow * window, int key, int scancode, int action, int mods) {
    if (action == GLFW_PRESS) {
        switch (key) {
            case GLFW_KEY_ESCAPE:
                glfwSetWindowShouldClose(window, true);
                break;
            default:
                // unimplemented key code
                break;
        }
    }
}

int main(int argc, char ** argv) {
    auto window = share::glfw_util::initGraphics(DEFAULT_WINDOW_WIDTH, DEFAULT_WINDOW_HEIGHT, "Gradients");

    glfwSetKeyCallback(window, keyCallback);

    std::cout << "OpenGL Version: " << glGetString(GL_VERSION)
        << "\nOpenGL Renderer: " << glGetString(GL_RENDERER)
        << "\nOpenGL Shading Language Version: " << glGetString(GL_SHADING_LANGUAGE_VERSION)
        << std::endl;

    struct BackgroundProgramT {
        GLuint program;
        GLuint vao;
    } background;    

    {
        auto vsh = share::gl_util::newShader(GL_VERTEX_SHADER, "data/shaders/full_screen_quad.vert");
        auto fsh = share::gl_util::newShader(GL_FRAGMENT_SHADER, "data/shaders/full_screen_quad.frag");
        auto shaders = std::vector<GLuint>();

        shaders.push_back(vsh);
        shaders.push_back(fsh);

        background.program = share::gl_util::newProgram(shaders);

        glDeleteShader(fsh);
        glDeleteShader(vsh);

        glCreateVertexArrays(1, &background.vao);
    }

    struct GradientProgramT {
        GLuint program;
        GLuint vBuffer;
        GLuint uBuffer;
        GLuint vao;
        
        GLsizei nGradients;
    } gradient;

    {
        auto vsh = share::gl_util::newShader(GL_VERTEX_SHADER, "data/shaders/gradient.vert");
        auto fsh = share::gl_util::newShader(GL_FRAGMENT_SHADER, "data/shaders/gradient.frag");
        auto shaders = std::vector<GLuint> ();

        shaders.push_back(vsh);
        shaders.push_back(fsh);

        gradient.program = share::gl_util::newProgram(shaders);

        glDeleteShader(fsh);
        glDeleteShader(vsh);

        glCreateVertexArrays(1, &gradient.vao);        
        
        glEnableVertexArrayAttrib(gradient.vao, 0);
        glVertexArrayAttribFormat(gradient.vao, 0, 2, GL_FLOAT, false, 0);
        glVertexArrayAttribBinding(gradient.vao, 0, 0);

        glEnableVertexArrayAttrib(gradient.vao, 1);
        glVertexArrayAttribFormat(gradient.vao, 1, 4, GL_UNSIGNED_BYTE, true, 8);
        glVertexArrayAttribBinding(gradient.vao, 1, 0);

        auto vertices = std::vector<GradientVertex>();

        {
            auto stops = std::vector<ColorStop>();

            stops.push_back({0.0F, {0xFF, 0x00, 0xFF, 0xFF}});
            stops.push_back({0.25F, {0x00, 0xFF, 0xFF, 0x7F}});
            stops.push_back({0.5F, {0xFF, 0xFF, 0x00, 0x3F}});
            stops.push_back({1.0F, {0x00, 0x00, 0x00, 0x00}});

            radial(vertices, {320.0F, 240.0F}, 256.0F, stops);
        }

        {
            auto stops = std::vector<ColorStop>();

            stops.push_back({0.0F, {0xFF, 0x00, 0x00, 0xFF}});
            stops.push_back({0.5F, {0x00, 0xFF, 0x00, 0xFF}});
            stops.push_back({1.0F, {0x00, 0x00, 0xFF, 0xFF}});

            hardLinear(vertices, {64, 64}, {128, 128}, {0.0F, 0.0F}, {1.0F, 1.0F}, stops);
        }

        glCreateBuffers(1, &gradient.vBuffer);
        glNamedBufferStorage(gradient.vBuffer, vertices.size() * sizeof(GradientVertex), vertices.data(), 0);

        gradient.nGradients = vertices.size();            
    }

    {
        auto mProj = glm::ortho(
                0.0F, static_cast<float> (DEFAULT_WINDOW_WIDTH), 
                static_cast<float> (DEFAULT_WINDOW_HEIGHT), 0.0F, 
                0.0F, 1.0F);

        glCreateBuffers(1, &gradient.uBuffer);
        glNamedBufferStorage(gradient.uBuffer, sizeof(float) * 16, glm::value_ptr(mProj), 0);
    }

    auto imageFile = share::ro_file<share::image> ("data/images/environment.png");
    auto image = share::gl_util::newTexture2D(imageFile.data());

    glEnable(GL_BLEND);
    glBlendEquationSeparate(GL_FUNC_ADD, GL_FUNC_ADD);
    glBlendFuncSeparate(GL_ONE, GL_ONE_MINUS_SRC_ALPHA, GL_ONE, GL_ONE_MINUS_SRC_ALPHA);    

    // enable for debug
    //glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

    while (!glfwWindowShouldClose(window)) {
        {
            auto frameWidth = int(1);
            auto frameHeight = int(1);

            glfwGetFramebufferSize(window, &frameWidth, &frameHeight);

            glViewport(0, 0, frameWidth, frameHeight);
        }

        glClear(GL_COLOR_BUFFER_BIT);        

        glUseProgram(background.program);        
        glBindTextureUnit(0, image);

        glBindVertexArray(background.vao);
        glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

        glUseProgram(gradient.program);
        glBindBufferBase(GL_UNIFORM_BUFFER, 0, gradient.uBuffer);

        glBindVertexArray(gradient.vao);
        glVertexArrayVertexBuffer(gradient.vao, 0, gradient.vBuffer, 0, sizeof(GradientVertex));
        glDrawArrays(GL_TRIANGLE_STRIP, 0, gradient.nGradients);

        share::gl_util::checkError();

        glfwSwapBuffers(window);
        glfwPollEvents();
    }

    glDeleteTextures(1, &image);
    glDeleteBuffers(1, &gradient.uBuffer);
    glDeleteBuffers(1, &gradient.vBuffer);    
    glDeleteVertexArrays(1, &gradient.vao);
    glDeleteProgram(gradient.program);
    glDeleteVertexArrays(1, &background.vao);
    glDeleteProgram(background.program);

    glfwDestroyWindow(window);

    glfwTerminate();
    
    return 0;
}
