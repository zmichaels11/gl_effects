#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include <cstring>

#include <iostream>
#include <random>

#include "share/gl_util.hpp"
#include "share/glfw_util.hpp"
#include "share/ro_file.hpp"

constexpr auto DEFAULT_WINDOW_WIDTH = std::size_t(640);
constexpr auto DEFAULT_WINDOW_HEIGHT = std::size_t(480);
constexpr auto MAX_TILES_ACROSS = std::size_t(16);
constexpr auto MAX_TILES_DOWN = std::size_t(16);
constexpr auto MAX_TILES = MAX_TILES_ACROSS * MAX_TILES_DOWN;
constexpr auto DEFAULT_TILE_WIDTH = float(32.0F);
constexpr auto DEFAULT_TILE_HEIGHT = float(32.0F);

struct __attribute__ ((packed)) TileLayerData {
    struct __attribute__ ((packed)) TileSizeT {
        float width, height;
    } tileSize;

    float padding[2];

    float projection[16];
};

void keyCallback(GLFWwindow * window, int key, int scancode, int action, int mods) {
    if (action == GLFW_PRESS) {
        switch (key) {
            case GLFW_KEY_ESCAPE:
                glfwSetWindowShouldClose(window, true);
                break;
            default:
                // unimplemented key code
                break;
        }
    }
}

int main(int argc, char ** argv) {
    auto window = share::glfw_util::initGraphics(DEFAULT_WINDOW_WIDTH, DEFAULT_WINDOW_HEIGHT, "Tilemap");

    glfwSetKeyCallback(window, keyCallback);

    std::cout << "OpenGL Version: " << glGetString(GL_VERSION)
        << "\nOpenGL Renderer: " << glGetString(GL_RENDERER)
        << "\nOpenGL Shading Language Version: " << glGetString(GL_SHADING_LANGUAGE_VERSION)
        << std::endl;

    auto program = GLuint(0);
    auto uBuffer = GLuint(0);
    auto vao = GLuint(0);
    auto tileMap = GLuint(0);
    auto tileSheet = GLuint(0); 

    {
        auto vsh = share::gl_util::newShader(GL_VERTEX_SHADER, "data/shaders/tiles.vert");
        auto gsh = share::gl_util::newShader(GL_GEOMETRY_SHADER, "data/shaders/tiles.geom");
        auto fsh = share::gl_util::newShader(GL_FRAGMENT_SHADER, "data/shaders/tiles.frag");
        auto shaders = std::vector<GLuint>();

        shaders.push_back(vsh);
        shaders.push_back(gsh);
        shaders.push_back(fsh);

        program = share::gl_util::newProgram(shaders);

        glDeleteShader(fsh);
        glDeleteShader(gsh);
        glDeleteShader(vsh);        

        glCreateVertexArrays(1, &vao);
    }

    {
        auto mProj = glm::ortho(
            0.0F, static_cast<float> (DEFAULT_WINDOW_WIDTH),
            static_cast<float> (DEFAULT_WINDOW_HEIGHT), 0.0F,
            0.0F, 1.0F);

        auto tld = TileLayerData();

        tld.tileSize.width = DEFAULT_TILE_WIDTH;
        tld.tileSize.height = DEFAULT_TILE_HEIGHT;
        
        std::memcpy(tld.projection, glm::value_ptr(mProj), sizeof(float) * 16);

        glCreateBuffers(1, &uBuffer);
        glNamedBufferStorage(uBuffer, sizeof(TileLayerData), &tld, 0);
    }

    {
        auto tile0File = share::ro_file<share::image> ("data/images/tiles/castle0.png");
        auto tile1File = share::ro_file<share::image> ("data/images/tiles/dirt0.png");

        auto pTiles = std::vector<const share::image * > ();

        pTiles.push_back(nullptr);
        pTiles.push_back(&tile0File.data());
        pTiles.push_back(&tile1File.data());

        tileSheet = share::gl_util::newTexture2DArray(pTiles);

        glCreateTextures(GL_TEXTURE_2D, 1, &tileMap);
        glTextureStorage2D(tileMap, 1, GL_R8, MAX_TILES_ACROSS, MAX_TILES_DOWN);

        auto tileMapData = std::make_unique<unsigned char[]> (MAX_TILES);

        auto rng = std::default_random_engine();
        auto dist = std::uniform_int_distribution<unsigned char> (0, 2);

        for (std::size_t j = 0; j < MAX_TILES_DOWN; j++) {
            for (std::size_t i = 0; i < MAX_TILES_ACROSS; i++) {                
                tileMapData[j * MAX_TILES_ACROSS + i] = dist(rng);
            }
        }

        glTextureSubImage2D(
            tileMap, 
            0, 0, 0, 
            MAX_TILES_ACROSS, MAX_TILES_DOWN, 
            GL_RED, GL_UNSIGNED_BYTE, 
            tileMapData.get());
    }

    share::gl_util::checkError();

    while (!glfwWindowShouldClose(window)) {
        {
            auto frameWidth = int(1);
            auto frameHeight = int(1);

            glfwGetFramebufferSize(window, &frameWidth, &frameHeight);

            glViewport(0, 0, frameWidth, frameHeight);
        }
        
        glClear(GL_COLOR_BUFFER_BIT);

        glUseProgram(program);
        glBindBufferBase(GL_UNIFORM_BUFFER, 0, uBuffer);        
        glBindTextureUnit(0, tileMap);
        glBindTextureUnit(1, tileSheet);

        glBindVertexArray(vao);
        glDrawArrays(GL_POINTS, 0, MAX_TILES);

        share::gl_util::checkError();

        glfwSwapBuffers(window);
        glfwPollEvents();
    }

    glDeleteTextures(1, &tileMap);
    glDeleteTextures(1, &tileSheet);
    glDeleteBuffers(1, &uBuffer);
    glDeleteVertexArrays(1, &vao);
    glDeleteProgram(program);

    glfwDestroyWindow(window);

    glfwTerminate();
    return 0;
}