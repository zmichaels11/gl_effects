#include "share/glew_exception.hpp"

namespace share {
    const char * glew_exception::what() const throw() {
        return _msg.c_str();
    }
}