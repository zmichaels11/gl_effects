#include "share/image.hpp"

#include <iostream>
#include <exception>
#include <sstream>
#include <string>
#include <utility>

#include "share/io_exception.hpp"

#define STBI_NO_STDIO
#define STBI_ONLY_PNG
#define STB_IMAGE_STATIC
#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"

namespace share {
    namespace {        
        class stbi_exception : io_exception {            
        public:
            stbi_exception(const std::string& msg) noexcept:
                io_exception(msg) {}
        };

        class image_format_exception : std::exception {
            std::string _msg;

        public:
            image_format_exception(const std::string& msg) noexcept:
                _msg(msg) {}

            virtual const char * what() const throw();
        };

        struct stb_image_resources : pimpl_resources {
            stbi_uc * _data;
            int _x, _y, _channels;

            virtual ~stb_image_resources();

            stb_image_resources(const void * data, std::size_t length);
        };
    }

    image::image(image&& other) {
        _pResources = std::move(other._pResources);
    }

    image::image(const void * imgdata, std::size_t length)  {
        _pResources = std::make_unique<stb_image_resources> (imgdata, length);        
    }

    image& image::operator= (image&& other) {
        std::swap(_pResources, other._pResources);

        return *this;
    }

    GLenum image::formatType() const {
        auto f = format();
        auto t = type();

        switch (f) {
            case GL_RED: {
                switch (t) {
                    case GL_UNSIGNED_BYTE:
                        return GL_R8;
                    default:
                        break;
                }
            } break;
            case GL_RG: {
                switch (t) {
                    case GL_UNSIGNED_BYTE:
                        return GL_RG8;
                    default:
                        break;
                } 
            } break;
            case GL_RGB: {
                switch (t) {
                    case GL_UNSIGNED_BYTE:
                        return GL_RGB8;
                    default:
                        break;
                }
            } break;
            case GL_RGBA: {
                switch (t) {
                    case GL_UNSIGNED_BYTE:
                        return GL_RGBA8;
                    default:
                        break;
                }
            } break;
            default:
                break;
        }

        throw image_format_exception("Invalid format, type pair!");
    }

    GLenum image::format() const {
        auto pRes = static_cast<const stb_image_resources * > (_pResources.get());

        switch (pRes->_channels) {
            case 1:
                return GL_RED;
            case 2:
                return GL_RG;
            case 3:
                return GL_RGB;
            case 4:
                return GL_RGBA;
            default:
                throw image_format_exception("Invalid number of channels in image!");
                break;
        }
    }

    GLenum image::type() const {
        return GL_UNSIGNED_BYTE;
    }

    std::size_t image::width() const noexcept {
        auto pRes = static_cast<const stb_image_resources * > (_pResources.get());

        return static_cast<std::size_t> (pRes->_x);
    }

    std::size_t image::height() const noexcept {
        auto pRes = static_cast<const stb_image_resources * > (_pResources.get());

        return static_cast<std::size_t> (pRes->_y);
    }

    const void * image::data() const noexcept {
        auto pRes = static_cast<const stb_image_resources * > (_pResources.get());

        return reinterpret_cast<const void * > (pRes->_data);
    }

    namespace {
        stb_image_resources::~stb_image_resources() {
            if (_data) {
                stbi_image_free(_data);
                _data = nullptr;
            }            
        }

        stb_image_resources::stb_image_resources(
            const void * data, 
            std::size_t length) {

            _data = stbi_load_from_memory(
                reinterpret_cast<const stbi_uc * > (data), static_cast<int> (length),
                &_x, &_y, &_channels, 0);

            if (_data == nullptr) {
                auto errmsg = std::stringstream();

                errmsg << "Error reading image: "
                    << stbi_failure_reason();

                throw stbi_exception(errmsg.str());
            }
        }

        const char * image_format_exception::what() const throw() {
            return _msg.c_str();
        }
    }
}