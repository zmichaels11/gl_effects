#include "share/gl_util.hpp"

#include <algorithm>
#include <limits>
#include <memory>
#include <sstream>

#include "share/opengl_exception.hpp"
#include "share/ro_file.hpp"

namespace share {
    namespace {
        class shader_compile_exception : opengl_exception {
        public:
            shader_compile_exception(const std::string& msg) noexcept:
                opengl_exception(msg) {}
        };

        class program_link_exception : opengl_exception {
        public:
            program_link_exception(const std::string& msg) noexcept:
                opengl_exception(msg) {}
        };

        class invalid_enum_exception : opengl_exception {
        public:
            invalid_enum_exception() noexcept:
                opengl_exception("Invalid Enum Exception: An unacceptable value is specified for an enumerated argument.") {}
        };

        class invalid_value_exception : opengl_exception {
        public:
            invalid_value_exception() noexcept:
                opengl_exception("Invalid Value Exception: A numeric argument is out of range.") {}
        };

        class invalid_operation_exception : opengl_exception {
        public:
            invalid_operation_exception() noexcept:
                opengl_exception("Invalid Operation Exception: The specified operation is not allowed in the current state.") {}
        };

        class stack_overflow_exception : opengl_exception {
        public:
            stack_overflow_exception() noexcept:
                opengl_exception("Stack Overflow Exception: This function would cause a stack overflow.") {}
        };

        class stack_underflow_exception : opengl_exception {
        public:
            stack_underflow_exception() noexcept:
                opengl_exception("Stack Underflow Exception: This function would cause a stack underflow.") {}
        };

        class out_of_memory_exception : opengl_exception {
        public:
            out_of_memory_exception() noexcept:
                opengl_exception("There is not enough memory left to execute the function.") {}
        };

        class unknown_exception : opengl_exception {
        public:
            unknown_exception(GLenum err) noexcept;
        };

        class uniform_location_not_found_exception : opengl_exception {
        public:
            uniform_location_not_found_exception(const char * uloc) noexcept;
        };

        class uniform_block_index_not_found_exception : opengl_exception {
        public:
            uniform_block_index_not_found_exception(const char * blockName) noexcept;
        };

        class invalid_image_size_exception : opengl_exception {
        public:
            invalid_image_size_exception() noexcept:
                opengl_exception("Image was defined with invalid size(s)!") {}
        };

        class incompatible_images_exception : opengl_exception {
        public:
            incompatible_images_exception() noexcept:
                opengl_exception("One or more images is incompatible with the format or type!") {}
        };
    }

    void gl_util::ortho(float * mat, float left, float right, float bottom, float top, float near, float far) noexcept {
        mat[0] = 2.0F / (right - left);
        mat[5] = 2.0F / (top - bottom);
        mat[10] = -2.0F / (far - near);
        mat[12] = -(right + left) / (right - left);
        mat[13] = -(top + bottom) / (top - bottom);
        mat[14] = -(far + near) / (far - near);
        mat[15] = 1.0F;
        
        mat[1] = mat[2] = mat[3] = 0.0F;
        mat[4] = mat[6] = mat[7] = 0.0F;
        mat[8] = mat[9] = mat[11] = 0.0F;
    }

    GLuint gl_util::newTexture2D(const share::baked_font& font) {
        auto out = GLuint(0);
        auto width = static_cast<GLsizei> (font.width());
        auto height = static_cast<GLsizei> (font.height());

        glCreateTextures(GL_TEXTURE_2D, 1, &out);
        
        glTextureParameteri(out, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glTextureParameteri(out, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glTextureParameteri(out, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glTextureParameteri(out, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

        glTextureStorage2D(out, 1, font.formatType(), width, height);
        glTextureSubImage2D(out, 0, 0, 0, width, height, font.format(), font.type(), font.data());
        
        checkError();

        return out;
    }

    GLuint gl_util::newTexture2D(const share::image& image) {
        auto out = GLuint(0);
        auto width = static_cast<GLsizei> (image.width());
        auto height = static_cast<GLsizei> (image.height());

        glCreateTextures(GL_TEXTURE_2D, 1, &out);

        glTextureParameteri(out, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glTextureParameteri(out, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glTextureParameteri(out, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glTextureParameteri(out, GL_TEXTURE_WRAP_T, GL_REPEAT);

        glTextureStorage2D(out, 1, image.formatType(), width, height);
        glTextureSubImage2D(out, 0, 0, 0, width, height, image.format(), image.type(), image.data());

        checkError();

        return out;
    }

    GLuint gl_util::newTexture2DArray(const std::vector<const share::image * >& images) {
        auto out = GLuint(0);
        auto width = GLsizei(0);
        auto height = GLsizei(0);
        auto format = GLenum(0);
        auto formatType = GLenum(0);
        auto layers = static_cast<GLsizei> (images.size());

        for (decltype(layers) i = 0; i < layers; i++) {
            auto pImage = images[i];

            if (pImage != nullptr) {
                width = std::max(width, static_cast<GLsizei> (pImage->width()));
                height = std::max(height, static_cast<GLsizei> (pImage->height()));
                
                if (formatType == 0) {
                    formatType = pImage->formatType();
                    format = pImage->format();
                } else if (formatType != pImage->formatType()) {
                    throw incompatible_images_exception();
                }                
            }            
        }

        if (width == 0 || height == 0) {
            throw invalid_image_size_exception();
        }

        glCreateTextures(GL_TEXTURE_2D_ARRAY, 1, &out);
        glTextureStorage3D(out, 1, formatType, width, height, layers);
        
        for (decltype(layers) i = 0; i < layers; i++) {
            auto pImage = images[i];

            if (pImage == nullptr) {
                auto channels = int(0);

                switch (format) {
                    case GL_RED:
                        channels = 1;
                        break;
                    case GL_RG:
                        channels = 2;
                        break;
                    case GL_RGB:
                        channels = 3;
                        break;
                    case GL_RGBA:
                        channels = 4;
                        break;
                    default:
                        continue;
                }

                auto tmp = std::make_unique<unsigned char[]> (width * height * channels);

                glTextureSubImage3D(
                    out, 0,
                    0, 0, 0,
                    width, height, 1,
                    format, GL_UNSIGNED_BYTE,
                    tmp.get());
            } else {                
                glTextureSubImage3D(
                    out, 0, 
                    0, 0, i, 
                    pImage->width(), pImage->height(), 1, 
                    format, pImage->type(), 
                    pImage->data());
            }            
        }

        checkError();

        return out;
    }

    GLuint gl_util::getCheckedUniformBlockIndex(GLuint program, const char * blockName) {
        auto out = glGetUniformBlockIndex(program, reinterpret_cast<const GLchar * > (blockName));

        if (out == std::numeric_limits<GLuint>::max()) {
            throw uniform_block_index_not_found_exception(blockName);
        }

        return out;
    }

    GLint gl_util::getCheckedUniformLocation(GLuint program, const char * uname) {
        auto out = glGetUniformLocation(program, reinterpret_cast<const GLchar * > (uname));        

        checkError();

        if (out == -1) {
            throw uniform_location_not_found_exception(uname);
        }

        return out;
    }

    void gl_util::checkError() {
        auto err = glGetError();

        switch (err) {
            case GL_NO_ERROR:
                break;
            case GL_INVALID_ENUM:
                throw invalid_enum_exception();
                break;
            case GL_INVALID_VALUE:
                throw invalid_value_exception();
                break;
            case GL_INVALID_OPERATION:
                throw invalid_operation_exception();
                break;
            case GL_STACK_OVERFLOW:
                throw stack_overflow_exception();
                break;
            case GL_STACK_UNDERFLOW:
                throw stack_underflow_exception();
                break;
            case GL_OUT_OF_MEMORY:
                throw out_of_memory_exception();
                break;
            default:
                throw unknown_exception(err);
                break;
        }
    }

    GLuint gl_util::newProgram(const std::vector<GLuint>& shaders) {
        auto out = glCreateProgram();

        for (auto const& shader : shaders) {
            glAttachShader(out, shader);
        }

        glLinkProgram(out);

        GLint status = GL_FALSE;

        glGetProgramiv(out, GL_LINK_STATUS, &status);

        for (auto const& shader : shaders) {
            glDetachShader(out, shader);
        }

        checkError();

        if (status == GL_FALSE) {
            GLint logLength = 0;

            glGetProgramiv(out, GL_INFO_LOG_LENGTH, &logLength);

            auto log = std::make_unique<GLchar[]> (logLength);

            glGetProgramInfoLog(out, logLength, nullptr, log.get());

            auto errmsg = std::stringstream();

            errmsg << "Link error: " << log.get();

            throw program_link_exception(errmsg.str());
        }        

        return out;
    }

    GLuint gl_util::newShader(GLenum type, const char * pFilename) {
        auto src = share::ro_file<std::string> (pFilename).data();
        auto out = glCreateShader(type);
        auto str = static_cast<const GLchar * > (src.c_str());
        auto len = static_cast<GLint> (src.length());
        
        glShaderSource(out, 1, &str, &len);
        glCompileShader(out);

        auto status = GLint(GL_FALSE);

        glGetShaderiv(out, GL_COMPILE_STATUS, &status);

        checkError();

        if (status == GL_FALSE) {
            auto logLength = GLint(0);

            glGetShaderiv(out, GL_INFO_LOG_LENGTH, &logLength);

            auto log = std::make_unique<GLchar[]> (logLength);

            glGetShaderInfoLog(out, logLength, nullptr, log.get());

            auto errmsg = std::stringstream();

            errmsg << "Compile error: " << log.get();

            throw shader_compile_exception(errmsg.str());
        }

        return out; 
    }

    std::uint16_t gl_util::unormUI16(float value) noexcept {
        return static_cast<std::uint16_t> (value * std::numeric_limits<std::uint16_t>::max());
    }

    std::size_t gl_util::alignUp(std::size_t val, std::size_t align) noexcept {
        return (val + align - 1) / align * align;
    }

    namespace {
        unknown_exception::unknown_exception(GLenum err) noexcept {
            auto errmsg = std::stringstream();

            errmsg << "Unknown OpenGL Error: 0x" << std::hex << err;

            _msg = errmsg.str();
        }

        uniform_location_not_found_exception::uniform_location_not_found_exception(const char * uloc) noexcept {
            auto errmsg = std::stringstream();

            errmsg << "Uniform location \"" << uloc << "\" not found!";

            _msg = errmsg.str();
        }

        uniform_block_index_not_found_exception::uniform_block_index_not_found_exception(const char * blockName) noexcept {
            auto errmsg = std::stringstream();

            errmsg << "Uniform block \"" << blockName << "\" not found!";

            _msg = errmsg.str();
        }
    }
}
