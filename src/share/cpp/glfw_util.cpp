#include "share/glfw_util.hpp"

#include "share/glew_exception.hpp"
#include "share/glfw_exception.hpp"

#include <iostream>

namespace share {
    namespace {
        void _messageCallback(
            GLenum source, GLenum type,
            GLuint id, GLenum severity,
            GLsizei length,
            const GLchar * message,
            const void * userParam);
    }

    GLFWwindow * glfw_util::initGraphics(
        int width, int height,
        const char * title) {

        if (!glfwInit()) {
            throw glfw_exception("Unable to initialize GLFW!");
        }

        glfwWindowHint(GLFW_CLIENT_API, GLFW_OPENGL_API);
        glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
        glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
        glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 5);

        auto window = glfwCreateWindow(width, height, title, nullptr, nullptr);

        if (window == nullptr) {
            throw glfw_exception("Unable to create GLFW window!");
        }

        glfwMakeContextCurrent(window);

        {
            glewExperimental = GL_TRUE;
            
            auto err = glewInit();

            if (GLEW_OK != err) {
                auto errmsg = reinterpret_cast<const char * > (glewGetErrorString(err));

                throw glew_exception(errmsg);
            }
        }

        glEnable(GL_DEBUG_OUTPUT);
        glDebugMessageCallback(_messageCallback, nullptr);

        while (glGetError() != GL_NO_ERROR);        

        return window;        
    }

    namespace {
        void _messageCallback(
            GLenum source, GLenum type,
            GLuint id, GLenum severity,
            GLsizei length,
            const GLchar * message,
            const void * userParam) {

            if (severity == GL_DEBUG_SEVERITY_NOTIFICATION) {
                return;
            }

            switch (type) {
                case GL_DEBUG_TYPE_ERROR:
                    std::cerr << "ERROR:"
                        << " type: 0x" << std::hex << type
                        << " severity: 0x" << severity
                        << " message: " << message << std::endl;
                    break;                
                default:
                    std::cout << "DEBUG:"
                        << " type: 0x" << std::hex << type
                        << " severity: 0x" << severity
                        << " message: " << message << std::endl;
                    break;
            }
        }
    }
}