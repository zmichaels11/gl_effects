#include "share/baked_font.hpp"

#define STB_TRUETYPE_IMPLEMENTATION
#include "stb_truetype.h"

#include <memory>
#include <vector>

#include "share/gl_util.hpp"
#include "share/ro_file.hpp"

namespace share {
    namespace {
        using ttf_bufferT = std::vector<unsigned char>;

        struct baked_font_resources : pimpl_resources {
            std::unique_ptr<stbtt_bakedchar[]> _cdata;
            std::unique_ptr<unsigned char[]> _image;
            std::size_t _width, _height;
            int _firstCharacter;

            baked_font_resources(const char * filename);
        };
    }

    baked_font::baked_font(const char * fileName) {
        _pResources = std::make_unique<baked_font_resources> (fileName);
    }

    std::vector<baked_font::quad> baked_font::encode(const int * text, std::size_t nCharacters) noexcept {
        auto pRes = static_cast<const baked_font_resources * > (_pResources.get());

        auto x = float(0.0F);
        auto y = float(0.0F);
        auto out = std::vector<quad>();

        out.reserve(nCharacters);

        for (std::size_t i = 0; i < nCharacters; i++) {
            auto q = stbtt_aligned_quad();
            auto c = quad();

            stbtt_GetBakedQuad(
                pRes->_cdata.get(), 
                static_cast<int> (pRes->_width), static_cast<int> (pRes->_height), 
                text[i] - pRes->_firstCharacter, 
                &x, &y,
                &q, 1);

            c.upperLeft = {q.x0, q.y0, gl_util::unormUI16(q.s0), gl_util::unormUI16(q.t0)};
            c.lowerRight = {q.x1, q.y1, gl_util::unormUI16(q.s1), gl_util::unormUI16(q.t1)};

            out.push_back(c);
        }

        return out;
    }

    std::size_t baked_font::width() const noexcept {
        auto pRes = static_cast<const baked_font_resources * > (_pResources.get());

        return pRes->_width;
    }

    std::size_t baked_font::height() const noexcept {
        auto pRes = static_cast<const baked_font_resources * > (_pResources.get());

        return pRes->_height;
    }

    const void * baked_font::data() const noexcept {
        auto pRes = static_cast<const baked_font_resources * > (_pResources.get());

        return reinterpret_cast<const void * > (pRes->_image.get());
    }

    GLenum baked_font::formatType() const noexcept {
        return GL_R8;
    }

    GLenum baked_font::format() const noexcept {
        return GL_RED;
    }

    GLenum baked_font::type() const noexcept {
        return GL_UNSIGNED_BYTE;
    }

    namespace {
        constexpr auto DEFAULT_FIRST_CHARACTER = int(' ');
        constexpr auto DEFAULT_LAST_CHARACTER = int('z');
        constexpr auto DEFAULT_PIXEL_HEIGHT = 32.0F;
        constexpr auto MINIMUM_WIDTH = std::size_t(128);
        constexpr auto MINIMUM_HEIGHT = std::size_t(128);        

        baked_font_resources::baked_font_resources(const char * filename) {
            _width = MINIMUM_WIDTH;
            _height = MINIMUM_HEIGHT;

            auto fontFile = ro_file<ttf_bufferT>(filename);            
            auto nPixels = _width * _height;
            auto flipFlop = bool(false);
            auto ttfData = fontFile.data();
            auto nChars = static_cast<std::size_t> (DEFAULT_LAST_CHARACTER - DEFAULT_FIRST_CHARACTER + 1);

            _firstCharacter = DEFAULT_FIRST_CHARACTER;
            _cdata = std::make_unique<stbtt_bakedchar[]> (nChars);
            _image = std::make_unique<unsigned char[]> (nPixels);            

            auto result = stbtt_BakeFontBitmap(
                ttfData.data(), 0, DEFAULT_PIXEL_HEIGHT, 
                _image.get(), 
                _width, _height,
                DEFAULT_FIRST_CHARACTER, nChars, 
                _cdata.get());

            while (result < 0) {
                if (flipFlop) {
                    _width <<= 1;
                } else {
                    _height <<= 1;
                }

                flipFlop = !flipFlop;

                nPixels = _width * _height;
                _image = std::make_unique<unsigned char[]> (nPixels);

                result = stbtt_BakeFontBitmap(
                    ttfData.data(), 0, DEFAULT_PIXEL_HEIGHT,
                    _image.get(),
                    _width, _height,
                    DEFAULT_FIRST_CHARACTER, nChars, 
                    _cdata.get());
            }            
        }
    }
}