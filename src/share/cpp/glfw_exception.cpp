#include "share/glfw_exception.hpp"

namespace share {
    const char * glfw_exception::what() const throw() {
        return _msg.c_str();
    }
}
