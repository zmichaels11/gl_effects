#include "share/io_exception.hpp"

namespace share {
    const char * io_exception::what() const throw() {
        return _msg.c_str();
    }
}