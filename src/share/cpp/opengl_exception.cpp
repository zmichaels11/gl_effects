#include "share/opengl_exception.hpp"

namespace share {
    const char * opengl_exception::what() const throw() {
        return _msg.c_str();
    }
}