#include "share/ro_file.hpp"

#include <fstream>
#include <sstream>

#include "share/io_exception.hpp"

namespace share {
    namespace {
        class file_open_exception : io_exception {
        public:
            file_open_exception(const std::string& msg) noexcept:
                io_exception(msg) {}
        };
    }

    template<>
    ro_file<std::string>::ro_file(const char * pFilename) {
        auto in = std::ifstream(pFilename, std::ios::in | std::ios::ate);

        if (in.good()) {
            auto len = in.tellg();

            _data.reserve(len);
            
            in.seekg(0, std::ios::beg);

            _data.assign(std::istreambuf_iterator<char> (in), std::istreambuf_iterator<char>());            
        } else {
            auto errmsg = std::stringstream();

            errmsg << "Unable to open file: \""
                << pFilename
                << "\"";

            throw file_open_exception(errmsg.str());
        }
    }
    
    template<>
    ro_file<std::vector<char>>::ro_file(const char * pFilename) {
        auto in = std::ifstream(pFilename, std::ios::in | std::ios::binary | std::ios::ate);

        if (in.good()) {
            auto len = in.tellg();            

            _data.reserve(len);
            _data.resize(len);

            in.seekg(0, std::ios::beg);
            in.read(_data.data(), len);     
        } else {
            auto errmsg = std::stringstream();

            errmsg << "Unable to open file: \""
                << pFilename
                << "\"";

            throw file_open_exception(errmsg.str());
        }
    }

    template<>
    ro_file<std::vector<unsigned char>>::ro_file(const char * pFilename) {
        auto in = std::ifstream(pFilename, std::ios::in | std::ios::binary | std::ios::ate);

        if (in.good()) {
            auto len = in.tellg();            

            _data.reserve(len);
            _data.resize(len);

            in.seekg(0, std::ios::beg);
            in.read(reinterpret_cast<char * > (_data.data()), len);
        } else {
            auto errmsg = std::stringstream();

            errmsg << "Unable to open file: \""
                << pFilename
                << "\"";

            throw file_open_exception(errmsg.str());
        }
    }

    template<>
    ro_file<image>::ro_file(const char * pFilename) {
        auto raw = ro_file<std::vector<unsigned char>> (pFilename).data();

        _data = image(raw.data(), raw.size());
    }
}