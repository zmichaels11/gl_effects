#pragma once

#include <exception>
#include <string>

namespace share {
    class opengl_exception : std::exception {
    protected:
        std::string _msg;

    public:
        opengl_exception() noexcept:
            _msg("OpenGL Exception") {}
            
        opengl_exception(const std::string& msg) noexcept:
            _msg(msg) {}

        virtual const char * what() const throw();
    };
}