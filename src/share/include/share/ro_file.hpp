#pragma once

#include <string>
#include <vector>

#include "share/image.hpp"

namespace share {

    template<class StorageT>
    class ro_file {
        StorageT _data;

        ro_file(const ro_file<StorageT>&) = delete;

        ro_file<StorageT>& operator= (const ro_file<StorageT>&) = delete;

    public:
        ro_file(const char * pFilename);

        ro_file(ro_file<StorageT>&& other) {
            _data = std::move(other._data);
        }

        inline ro_file<StorageT>& operator=(ro_file<StorageT>&& other) {
            std::swap(_data, other._data);

            return *this;
        }

        inline const StorageT& data() const {
            return _data;
        }
    };    

    template<>
    ro_file<std::string>::ro_file(const char * pFilename);

    template<>
    ro_file<std::vector<char>>::ro_file(const char * pFilename);

    template<>
    ro_file<std::vector<unsigned char>>::ro_file(const char * pFilename);

    template<>
    ro_file<image>::ro_file(const char * pFilename);
}