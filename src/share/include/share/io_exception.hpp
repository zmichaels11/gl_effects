#pragma once

#include <exception>
#include <string>

namespace share {
    class io_exception : public std::exception {
    protected:
        std::string _msg;

    public:
        io_exception(const std::string& msg) noexcept :
            _msg(msg) {}

        virtual const char * what() const throw();
    };
}