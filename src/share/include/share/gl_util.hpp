#pragma once

#include <GL/glew.h>

#include <cstdint>

#include <vector>

#include "share/baked_font.hpp"
#include "share/image.hpp"
#include "share/opengl_exception.hpp"

namespace share {
    class gl_util {
        gl_util() = delete;

    public:
        static auto newShader(GLenum type, const char * pFilename) -> decltype(glCreateShader(type));

        static auto newProgram(const std::vector<decltype(newShader(0, nullptr))>& shaders) -> decltype(glCreateProgram());

        static void checkError();

        static auto getCheckedUniformLocation(GLuint program, const char * uname) -> decltype(glGetUniformLocation(0, nullptr));

        static auto getCheckedUniformBlockIndex(GLuint program, const char * blockName) -> decltype(glGetUniformBlockIndex(0, nullptr));

        static GLuint newTexture2D(const share::image&);

        static GLuint newTexture2D(const share::baked_font&);

        static GLuint newTexture2DArray(const std::vector<const share::image * >& images);

        static void ortho(float * mat, float left, float right, float bottom, float top, float near, float far) noexcept;

        static std::uint16_t unormUI16(float) noexcept;

        static std::size_t alignUp(std::size_t val, std::size_t align) noexcept;
    };
}