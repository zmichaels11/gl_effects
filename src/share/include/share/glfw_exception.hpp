#pragma once

#include <exception>
#include <string>

namespace share {
    class glfw_exception : std::exception {
        std::string _msg;

    public:
        glfw_exception(const std::string& msg) noexcept:
            _msg(msg) {}

        virtual const char * what() const throw();
    };
}