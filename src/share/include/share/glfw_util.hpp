#pragma once

#include <GL/glew.h>
#include <GLFW/glfw3.h>

namespace share {
    class glfw_util {
        glfw_util() = delete;
    
    public:
        static auto initGraphics(
            int width, int height,
            const char * title) -> decltype(glfwCreateWindow(width, height, title, nullptr, nullptr));
    };
}