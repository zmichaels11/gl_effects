#pragma once

#include <GL/glew.h>

#include <cstddef>

#include <memory>

#include "share/pimpl_resources.hpp"

namespace share {
    class image {
        std::unique_ptr<pimpl_resources> _pResources;

        image(const image&) = delete;

        image& operator= (const image&) = delete;

    public:
        image(image&& other);

        image() {}

        image(const void * imgdata, std::size_t length);

        image& operator= (image&& other);

        GLenum formatType() const;

        GLenum format() const;

        GLenum type() const;

        std::size_t width() const noexcept;

        std::size_t height() const noexcept;

        const void * data() const noexcept;
    };
}