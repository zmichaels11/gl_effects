#pragma once

#include <GL/glew.h>

#include <cstddef>
#include <cstdint>

#include <memory>
#include <vector>

#include "share/pimpl_resources.hpp"

namespace share {
    class baked_font {
        std::unique_ptr<pimpl_resources> _pResources;

    public:
        baked_font(const char * fileName);        

        std::size_t width() const noexcept;

        std::size_t height() const noexcept;

        const void * data() const noexcept;

        GLenum formatType() const noexcept;

        GLenum format() const noexcept;

        GLenum type() const noexcept;

        struct __attribute__ ((packed)) quad {
            struct __attribute__ ((packed)) Point {
                float x, y;
                std::uint16_t s, t;
            } upperLeft, lowerRight;
        };

        std::vector<quad> encode(const int * text, std::size_t nCharacters) noexcept;

        std::vector<quad> encode(const char * text, std::size_t nCharacters) noexcept {
            auto temp = std::make_unique<int[]> (nCharacters);

            for (std::size_t i = 0; i < nCharacters; i++) {
                temp[i] = static_cast<int> (text[i]);
            }

            return encode(temp.get(), nCharacters);
        }
    };
}