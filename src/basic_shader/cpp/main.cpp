#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include <iostream>

#include "share/gl_util.hpp"
#include "share/glfw_util.hpp"
#include "share/ro_file.hpp"

struct __attribute__ ((packed)) ColorT {
    float r, g, b, a;
};

constexpr auto DEFAULT_WINDOW_WIDTH = std::size_t(640);
constexpr auto DEFAULT_WINDOW_HEIGHT = std::size_t(480);

void keyCallback(GLFWwindow * window, int key, int scancode, int action, int mods) {
    if (action == GLFW_PRESS) {
        switch (key) {
            case GLFW_KEY_ESCAPE:
                glfwSetWindowShouldClose(window, true);
                break;
            default:
                // unimplemented key code
                break;
        }
    }
}

int main(int argc, char ** argv) {
    auto window = share::glfw_util::initGraphics(DEFAULT_WINDOW_WIDTH, DEFAULT_WINDOW_HEIGHT, "Basic Shader");

    glfwSetKeyCallback(window, keyCallback);

    std::cout << "OpenGL Version: " << glGetString(GL_VERSION)
        << "\nOpenGL Renderer: " << glGetString(GL_RENDERER)
        << "\nOpenGL Shading Language Version: " << glGetString(GL_SHADING_LANGUAGE_VERSION)
        << std::endl;

    auto program = GLuint(0);
    auto uBuffer = GLuint(0);
    auto vao = GLuint(0);
    auto pColor = static_cast<ColorT * > (nullptr);

    {
        auto vsh = share::gl_util::newShader(GL_VERTEX_SHADER, "data/shaders/hello_shader.vert");
        auto fsh = share::gl_util::newShader(GL_FRAGMENT_SHADER, "data/shaders/hello_shader.frag");
        auto shaders = std::vector<GLuint>();

        shaders.push_back(vsh);
        shaders.push_back(fsh);

        program = share::gl_util::newProgram(shaders);

        glDeleteShader(fsh);
        glDeleteShader(vsh);

        glCreateBuffers(1, &uBuffer);
        glNamedBufferStorage(uBuffer, sizeof(float) * 4, nullptr, GL_MAP_WRITE_BIT | GL_MAP_PERSISTENT_BIT | GL_MAP_COHERENT_BIT);

        pColor = reinterpret_cast<ColorT * > (glMapNamedBufferRange(uBuffer, 0, sizeof(float) * 4, GL_MAP_WRITE_BIT | GL_MAP_PERSISTENT_BIT | GL_MAP_COHERENT_BIT));

        glCreateVertexArrays(1, &vao);
    }

    while (!glfwWindowShouldClose(window)) {
        pColor->r = 1.0F;
        pColor->g = 0.0F;
        pColor->b = 0.0F;
        pColor->a = 1.0F;

        glClear(GL_COLOR_BUFFER_BIT);

        glUseProgram(program);
        glBindBufferBase(GL_UNIFORM_BUFFER, 0, uBuffer);

        glBindVertexArray(vao);
        glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

        share::gl_util::checkError();

        glfwSwapBuffers(window);
        glfwPollEvents();
    }

    glUnmapNamedBuffer(uBuffer);
    glDeleteBuffers(1, &uBuffer);
    glDeleteVertexArrays(1, &vao);
    glDeleteProgram(program);

    glfwDestroyWindow(window);

    glfwTerminate();
    return 0;
}