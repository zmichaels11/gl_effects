#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include <iostream>

#include "share/gl_util.hpp"
#include "share/glfw_util.hpp"
#include "share/ro_file.hpp"

constexpr auto DEFAULT_WINDOW_WIDTH = std::size_t(640);
constexpr auto DEFAULT_WINDOW_HEIGHT = std::size_t(480);

void keyCallback(GLFWwindow * window, int key, int scancode, int action, int mods) {
    if (action == GLFW_PRESS) {
        switch (key) {
            case GLFW_KEY_ESCAPE:
                glfwSetWindowShouldClose(window, true);
                break;
            default:
                // unimplemented key code
                break;
        }
    }
}

int main(int argc, char ** argv) {
    auto window = share::glfw_util::initGraphics(DEFAULT_WINDOW_WIDTH, DEFAULT_WINDOW_HEIGHT, "Background");

    glfwSetKeyCallback(window, keyCallback);

    std::cout << "OpenGL Version: " << glGetString(GL_VERSION)
        << "\nOpenGL Renderer: " << glGetString(GL_RENDERER)
        << "\nOpenGL Shading Language Version: " << glGetString(GL_SHADING_LANGUAGE_VERSION)
        << std::endl;

    auto program = GLuint(0);
    auto vao = GLuint(0);    

    {
        auto vsh = share::gl_util::newShader(GL_VERTEX_SHADER, "data/shaders/full_screen_quad.vert");
        auto fsh = share::gl_util::newShader(GL_FRAGMENT_SHADER, "data/shaders/full_screen_quad.frag");
        auto shaders = std::vector<GLuint>();

        shaders.push_back(vsh);
        shaders.push_back(fsh);

        program = share::gl_util::newProgram(shaders);
        
        glDeleteShader(vsh);
        glDeleteShader(fsh);

        glCreateVertexArrays(1, &vao);
    }

    auto imageFile = share::ro_file<share::image> ("data/images/environment.png");
    auto image = share::gl_util::newTexture2D(imageFile.data());

    while (!glfwWindowShouldClose(window)) {
        {
            auto frameWidth = int(0);
            auto frameHeight = int(0);

            glfwGetFramebufferSize(window, &frameWidth, &frameHeight);
            glViewport(0, 0, frameWidth, frameHeight);
        }

        glClear(GL_COLOR_BUFFER_BIT);

        glUseProgram(program);
        glBindTextureUnit(0, image);

        glBindVertexArray(vao);
        glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

        share::gl_util::checkError();

        glfwSwapBuffers(window);
        glfwPollEvents();
    }

    glDeleteVertexArrays(1, &vao);
    glDeleteTextures(1, &image);
    glDeleteProgram(program);

    glfwDestroyWindow(window);

    glfwTerminate();
    return 0;
}