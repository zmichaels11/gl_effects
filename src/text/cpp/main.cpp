#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include <cstdint>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include <array>
#include <iostream>
#include <vector>

#include "share/baked_font.hpp"
#include "share/gl_util.hpp"
#include "share/glfw_util.hpp"
#include "share/ro_file.hpp"

struct __attribute__ ((packed)) Point {
    float x, y;
};

struct __attribute__ ((packed)) TexCoord {
    std::uint16_t u, v;
};

struct __attribute__ ((packed)) Color {
    std::uint8_t r, g, b, a;
};

struct __attribute__ ((packed)) Vertex {
    Point position;
    TexCoord texCoord;
    Color color;
};

void keyCallback(GLFWwindow * window, int key, int scancode, int action, int mods) {
    if (action == GLFW_PRESS) {
        switch (key) {
            case GLFW_KEY_ESCAPE:
                glfwSetWindowShouldClose(window, true);
                break;
            default:
                // unimplemented key code
                break;
        }
    }
}

constexpr auto DEFAULT_WINDOW_WIDTH = std::size_t(640);
constexpr auto DEFAULT_WINDOW_HEIGHT = std::size_t(480);

int main(int argc, char ** argv) {
    auto window = share::glfw_util::initGraphics(DEFAULT_WINDOW_WIDTH, DEFAULT_WINDOW_HEIGHT, "Basic Shader");

    glfwSetKeyCallback(window, keyCallback);

    std::cout << "OpenGL Version: " << glGetString(GL_VERSION)
        << "\nOpenGL Renderer: " << glGetString(GL_RENDERER)
        << "\nOpenGL Shading Language Version: " << glGetString(GL_SHADING_LANGUAGE_VERSION)
        << std::endl;

    auto program = GLuint(0);    
    auto vao = GLuint(0);
    auto vBuffer = GLuint(0);
    auto uBuffer = GLuint(0);
    auto font = GLuint(0);
    auto projection = std::array<float, 16>();    

    {
        auto vsh = share::gl_util::newShader(GL_VERTEX_SHADER, "data/shaders/text.vert");
        auto fsh = share::gl_util::newShader(GL_FRAGMENT_SHADER, "data/shaders/text.frag");
        auto shaders = std::vector<GLuint>();

        shaders.push_back(vsh);
        shaders.push_back(fsh);

        program = share::gl_util::newProgram(shaders);

        glDeleteShader(fsh);
        glDeleteShader(vsh);

        glCreateVertexArrays(1, &vao);
        
        glEnableVertexArrayAttrib(vao, 0);
        glVertexArrayAttribFormat(vao, 0, 2, GL_FLOAT, false, 0);
        glVertexArrayAttribBinding(vao, 0, 0);

        glEnableVertexArrayAttrib(vao, 1);
        glVertexArrayAttribFormat(vao, 1, 2, GL_UNSIGNED_SHORT, true, 8);
        glVertexArrayAttribBinding(vao, 1, 0);

        glEnableVertexArrayAttrib(vao, 2);
        glVertexArrayAttribFormat(vao, 2, 4, GL_UNSIGNED_BYTE, true, 12);
        glVertexArrayAttribBinding(vao, 2, 0);
    }

    auto nVertices = GLsizei(0);

    {
        auto bakedFont = share::baked_font("data/fonts/Roboto-Regular.ttf");
        
        font = share::gl_util::newTexture2D(bakedFont);

        auto quads = bakedFont.encode("Hello World!", 12);
        auto vertices = std::vector<Vertex>();

        for (auto&& quad : quads) {
            auto& upperLeft = quad.upperLeft;
            auto& lowerRight = quad.lowerRight;

            vertices.push_back({{upperLeft.x, upperLeft.y}, {upperLeft.s, upperLeft.t}, {0xFF, 0x00, 0x00, 0xFF}});

            if (vertices.size() > 1) {
                vertices.push_back(vertices.back());
            }

            vertices.push_back({{upperLeft.x, lowerRight.y}, {upperLeft.s, lowerRight.t}, {0xFF, 0x00, 0x00, 0xFF}});
            vertices.push_back({{lowerRight.x, upperLeft.y}, {lowerRight.s, upperLeft.t}, {0xFF, 0x00, 0x00, 0xFF}});
            vertices.push_back({{lowerRight.x, lowerRight.y}, {lowerRight.s, lowerRight.t}, {0xFF, 0x00, 0x00, 0xFF}});
            vertices.push_back(vertices.back());            
        }

        glCreateBuffers(1, &vBuffer);
        glNamedBufferStorage(vBuffer, vertices.size() * sizeof(Vertex), vertices.data(), 0);

        nVertices = static_cast<GLsizei> (vertices.size());
    }

    {
        auto mProj = glm::ortho(
            0.0F, static_cast<float> (DEFAULT_WINDOW_WIDTH), 
            static_cast<float> (DEFAULT_WINDOW_HEIGHT), 0.0F, 
            0.0F, 1.0F);

        auto mMV = glm::translate(glm::mat4x4(1.0F), glm::vec3(64.0F, 64.0F, 0.0F));

        auto mvp = mProj * mMV;

        glCreateBuffers(1, &uBuffer);
        glNamedBufferStorage(uBuffer, 16 * sizeof(float), glm::value_ptr(mvp), 0);
    }    

    glEnable(GL_BLEND);
    glBlendEquationSeparate(GL_FUNC_ADD, GL_FUNC_ADD);
    glBlendFuncSeparate(GL_ONE, GL_ONE_MINUS_SRC_ALPHA, GL_ONE, GL_ONE_MINUS_SRC_ALPHA);    

    while (!glfwWindowShouldClose(window)) {
        {
            auto frameWidth = int(1);
            auto frameHeight = int(1);

            glfwGetFramebufferSize(window, &frameWidth, &frameHeight);

            glViewport(0, 0, frameWidth, frameHeight);
        }
        
        glClear(GL_COLOR_BUFFER_BIT);

        glUseProgram(program);
        glBindBufferBase(GL_UNIFORM_BUFFER, 0, uBuffer);
        glBindTextureUnit(0, font);

        glVertexArrayVertexBuffer(vao, 0, vBuffer, 0, sizeof(Vertex));

        glBindVertexArray(vao);
        glDrawArrays(GL_TRIANGLE_STRIP, 0, nVertices);

        share::gl_util::checkError();

        glfwSwapBuffers(window);
        glfwPollEvents();
    }

    glDeleteBuffers(1, &uBuffer);
    glDeleteBuffers(1, &vBuffer);
    glDeleteVertexArrays(1, &vao);
    glDeleteProgram(program);

    glfwDestroyWindow(window);

    glfwTerminate();
    return 0;
}