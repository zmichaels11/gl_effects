#version 450 core

const vec2 VERTICES[4] = vec2[] (
    vec2(-1.0, -1.0), vec2(-1.0, 1.0),
    vec2(1.0, -1.0), vec2(1.0, 1.0));

void main() {
    gl_Position = vec4(VERTICES[gl_VertexID], 0.0, 1.0);
}