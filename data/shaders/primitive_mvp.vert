#version 450 core

layout (location = 0)   in vec3 vPosition;
layout (location = 1)   in vec4 vColor;

layout (location = 0)   out vec4 fColor;

layout (binding = 0, std140)    uniform Matrices {
    mat4 uProjection;
    mat4 uModelView;
};

void main() {
    fColor = vColor;
    gl_Position = uProjection * uModelView * vec4(vPosition, 1.0);
}