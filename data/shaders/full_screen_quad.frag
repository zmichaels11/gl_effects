#version 450 core

layout (location = 0)   in vec2 fTexCoord;

layout (location = 0)   out vec4 result;

layout (binding = 0)    uniform sampler2D uImage;

void main() {
    result = texture(uImage, fTexCoord);
    result.rgb *= result.a;
}