#version 450 core

const vec2 TEX_COORD[4] = vec2[] (
    vec2(0.0, 0.0), vec2(0.0, 1.0),
    vec2(1.0, 0.0), vec2(1.0, 1.0));

const vec2 VERTICES[4] = vec2[] (
    vec2(-1.0, 1.0), vec2(-1.0, -1.0),
    vec2(1.0, 1.0), vec2(1.0, -1.0));

layout (location = 0)   out vec2 fTexCoord;

void main() {
    fTexCoord = TEX_COORD[gl_VertexID];

    gl_Position = vec4(VERTICES[gl_VertexID], 0.0, 1.0);
}