#version 450 core

layout (location = 0)   in vec3 fTexCoord;

layout (location = 0)   out vec4 result;

layout (binding = 1)    uniform sampler2DArray uTilesheet;

void main() {
    result = texture(uTilesheet, fTexCoord);
    result.rgb *= result.a;
}