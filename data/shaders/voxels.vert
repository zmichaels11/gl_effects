#version 450 core

layout (location = 0)   out vec3 gFrontTopLeft;
layout (location = 1)   out vec3 gFrontTopRight;
layout (location = 2)   out vec3 gFrontBottomLeft;
layout (location = 3)   out vec3 gFrontBottomRight;
layout (location = 4)   out vec3 gBackTopLeft;
layout (location = 5)   out vec3 gBackTopRight;
layout (location = 6)   out vec3 gBackBottomLeft;
layout (location = 7)   out vec3 gBackBottomRight;
layout (location = 8)   out float gIndex;

layout (binding = 0, std140)    uniform VoxelData {
    vec3 uSize;
    float padding;
    mat4 uProjection;
};

layout (binding = 0)    uniform sampler3D uPointcloud;

void main() {
    ivec3 pointCloudSize = textureSize(uPointcloud, 0);    
    
    int vx = gl_VertexID / (pointCloudSize.y * pointCloudSize.z);
    int vy = (gl_VertexID - vx * pointCloudSize.y * pointCloudSize.z) / pointCloudSize.z;
    int vz = gl_VertexID - vx * pointCloudSize.y * pointCloudSize.z - vy * pointCloudSize.z;

    vec3 p = vec3(vx, vy, vz) * uSize;

    gFrontTopLeft = p + vec3(0.0, uSize.yz);
    gFrontTopRight = p + uSize;
    gFrontBottomLeft = p + vec3(0.0, 0.0, uSize.z);
    gFrontBottomRight = p + vec3(uSize.x, 0.0, uSize.z);

    gBackBottomLeft = p;
    gBackBottomRight = p + vec3(uSize.x, 0.0, 0.0);
    gBackTopLeft = p + vec3(0.0, uSize.y, 0.0);
    gBackTopRight = p + vec3(uSize.xy, 0.0);

    gIndex = texelFetch(uPointcloud, ivec3(vx, vy, vz), 0).r * 255.0;
}