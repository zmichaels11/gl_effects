#version 450 core

layout (points) in;
layout (triangle_strip, max_vertices=34) out;

layout (location = 0)   in vec3 gFrontTopLeft[];
layout (location = 1)   in vec3 gFrontTopRight[];
layout (location = 2)   in vec3 gFrontBottomLeft[];
layout (location = 3)   in vec3 gFrontBottomRight[];
layout (location = 4)   in vec3 gBackTopLeft[];
layout (location = 5)   in vec3 gBackTopRight[];
layout (location = 6)   in vec3 gBackBottomLeft[];
layout (location = 7)   in vec3 gBackBottomRight[];
layout (location = 8)   in float gIndex[];

layout (location = 0)    out vec3 fTexCoord;

layout (binding = 0, std140)    uniform VoxelData {
    vec3 uSize;
    float padding;
    mat4 uProjection;
};

void main() {
    if (gIndex[0] < 1.0) {
        return;
    }
    
    // top
    fTexCoord = vec3(0.0, 0.0, gIndex[0]);
    gl_Position = uProjection * vec4(gBackTopLeft[0], 1.0);
    EmitVertex();

    fTexCoord = vec3(0.0, 1.0, gIndex[0]);
    gl_Position = uProjection * vec4(gFrontTopLeft[0], 1.0);
    EmitVertex();

    fTexCoord = vec3(1.0, 0.0, gIndex[0]);
    gl_Position = uProjection * vec4(gBackTopRight[0], 1.0);
    EmitVertex();

    fTexCoord = vec3(1.0, 1.0, gIndex[0]);
    gl_Position = uProjection * vec4(gFrontTopRight[0], 1.0);
    EmitVertex();
    EmitVertex();

    // bottom
    fTexCoord = vec3(0.0, 0.0, gIndex[0]);
    gl_Position = uProjection * vec4(gFrontBottomLeft[0], 1.0);
    EmitVertex();
    EmitVertex();

    fTexCoord = vec3(0.0, 1.0, gIndex[0]);
    gl_Position = uProjection * vec4(gBackBottomLeft[0], 1.0);
    EmitVertex();

    fTexCoord = vec3(1.0, 0.0, gIndex[0]);
    gl_Position = uProjection * vec4(gFrontBottomRight[0], 1.0);
    EmitVertex();

    fTexCoord = vec3(1.0, 1.0, gIndex[0]);
    gl_Position = uProjection * vec4(gBackBottomRight[0], 1.0);
    EmitVertex();
    EmitVertex();

    // front
    fTexCoord = vec3(0.0, 0.0, gIndex[0]);
    gl_Position = uProjection * vec4(gFrontTopLeft[0], 1.0);
    EmitVertex();
    EmitVertex();

    fTexCoord = vec3(0.0, 1.0, gIndex[0]);
    gl_Position = uProjection * vec4(gFrontBottomLeft[0], 1.0);
    EmitVertex();

    fTexCoord = vec3(1.0, 0.0, gIndex[0]);
    gl_Position = uProjection * vec4(gFrontTopRight[0], 1.0);
    EmitVertex();

    fTexCoord = vec3(1.0, 1.0, gIndex[0]);
    gl_Position = uProjection * vec4(gFrontBottomRight[0], 1.0);
    EmitVertex();
    EmitVertex();

    // back
    fTexCoord = vec3(0.0, 0.0, gIndex[0]);
    gl_Position = uProjection * vec4(gBackTopRight[0], 1.0);
    EmitVertex();
    EmitVertex();

    fTexCoord = vec3(0.0, 1.0, gIndex[0]);
    gl_Position = uProjection * vec4(gBackBottomRight[0], 1.0);
    EmitVertex();

    fTexCoord = vec3(1.0, 0.0, gIndex[0]);
    gl_Position = uProjection * vec4(gBackTopLeft[0], 1.0);
    EmitVertex();

    fTexCoord = vec3(1.0, 1.0, gIndex[0]);
    gl_Position = uProjection * vec4(gBackBottomLeft[0], 1.0);
    EmitVertex();
    EmitVertex();

    // left
    fTexCoord = vec3(0.0, 0.0, gIndex[0]);
    gl_Position = uProjection * vec4(gBackTopLeft[0], 1.0);
    EmitVertex();
    EmitVertex();

    fTexCoord = vec3(0.0, 1.0, gIndex[0]);
    gl_Position = uProjection * vec4(gBackBottomLeft[0], 1.0);
    EmitVertex();

    fTexCoord = vec3(1.0, 0.0, gIndex[0]);
    gl_Position = uProjection * vec4(gFrontTopLeft[0], 1.0);
    EmitVertex();

    fTexCoord = vec3(1.0, 1.0, gIndex[0]);
    gl_Position = uProjection * vec4(gFrontBottomLeft[0], 1.0);
    EmitVertex();
    EmitVertex();

    // right
    fTexCoord = vec3(0.0, 0.0, gIndex[0]);
    gl_Position = uProjection * vec4(gFrontTopRight[0], 1.0);
    EmitVertex();
    EmitVertex();

    fTexCoord = vec3(0.0, 1.0, gIndex[0]);
    gl_Position = uProjection * vec4(gFrontBottomRight[0], 1.0);
    EmitVertex();

    fTexCoord = vec3(1.0, 0.0, gIndex[0]);
    gl_Position = uProjection * vec4(gBackTopRight[0], 1.0);
    EmitVertex();

    fTexCoord = vec3(1.0, 1.0, gIndex[0]);
    gl_Position = uProjection * vec4(gBackBottomRight[0], 1.0);
    EmitVertex();

    EndPrimitive();
}