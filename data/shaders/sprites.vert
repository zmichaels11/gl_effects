#version 450 core

layout (location = 0)   in vec2 vUpperLeft;
layout (location = 1)   in vec2 vUpperRight;
layout (location = 2)   in vec2 vLowerLeft;
layout (location = 3)   in float vFrameIndex;
layout (location = 4)   in vec2 vFrameSize;
layout (location = 5)   in int vColorIndex;

layout (location = 0)   out vec2 gUpperLeft;
layout (location = 1)   out vec2 gUpperRight;
layout (location = 2)   out vec2 gLowerLeft;
layout (location = 3)   out vec2 gLowerRight;
layout (location = 4)   out vec3 gTexCoord;
layout (location = 5)   out int gColorIndex;

void main() {
    gUpperLeft = vUpperLeft;
    gUpperRight = vUpperRight;
    gLowerLeft = vLowerLeft;
    gLowerRight = vUpperRight + (vLowerLeft - vUpperLeft);
    gTexCoord = vec3(vFrameSize, vFrameIndex);
    gColorIndex = vColorIndex;
}