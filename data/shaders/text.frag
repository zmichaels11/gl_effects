#version 450 core

layout (location = 0)   in vec2 fTexCoord;
layout (location = 1)   in vec4 fColor;

layout (location = 0)   out vec4 result;

layout (binding = 0)    uniform sampler2D uFont;

void main() {
    float alpha = fColor.a * texture(uFont, fTexCoord).r;

    result = vec4(fColor.rgb * alpha, alpha);
}