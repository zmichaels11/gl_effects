#version 450 core

layout (location = 0)   out vec4 result;

layout (binding = 0)    uniform ColorBlock {
    vec4 uColor;
};

void main() {
    result = uColor;
}