#version 450 core

layout (location = 0)   out vec2 gUpperLeft;
layout (location = 1)   out vec2 gUpperRight;
layout (location = 2)   out vec2 gLowerLeft;
layout (location = 3)   out vec2 gLowerRight;
layout (location = 4)   out float gIndex;

layout (binding = 0, std140)    uniform TileLayerData {
    vec2 uSize;
    vec2 padding;
    mat4 uProjection;
};

layout (binding = 0)    uniform sampler2D uTilemap;

void main() {
    ivec2 tilemapSize = textureSize(uTilemap, 0);
    int tx = gl_VertexID % tilemapSize.x;
    int ty = gl_VertexID / tilemapSize.x;

    vec2 vPos = vec2(tx, ty) * uSize;

    gUpperLeft = vPos;
    gUpperRight = vPos + vec2(uSize.x, 0.0);
    gLowerLeft = vPos + vec2(0.0, uSize.y);
    gLowerRight = vPos + uSize;

    gIndex = texelFetch(uTilemap, ivec2(tx, ty), 0).r * 255.0;    
}