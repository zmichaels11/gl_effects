#version 450

layout (points) in;
layout (triangle_strip, max_vertices = 4) out;

layout (location = 0)   in vec2 gUpperLeft[];
layout (location = 1)   in vec2 gUpperRight[];
layout (location = 2)   in vec2 gLowerLeft[];
layout (location = 3)   in vec2 gLowerRight[];
layout (location = 4)   in float gIndex[];

layout (location = 0)   out vec3 fTexCoord;

layout (binding = 0, std140)    uniform TileLayerData {
    vec2 uSize;
    vec2 padding;
    mat4 uProjection;
};

void main() {
    if (gIndex[0] < 1.0) {
        return;
    }
    
    fTexCoord = vec3(0.0, 0.0, gIndex[0]);
    gl_Position = uProjection * vec4(gUpperLeft[0], 0.0, 1.0);
    EmitVertex();

    fTexCoord = vec3(0.0, 1.0, gIndex[0]);
    gl_Position = uProjection * vec4(gLowerLeft[0], 0.0, 1.0);
    EmitVertex();

    fTexCoord = vec3(1.0, 0.0, gIndex[0]);
    gl_Position = uProjection * vec4(gUpperRight[0], 0.0, 1.0);
    EmitVertex();

    fTexCoord = vec3(1.0, 1.0, gIndex[0]);
    gl_Position = uProjection * vec4(gLowerRight[0], 0.0, 1.0);
    EmitVertex();

    EndPrimitive();
}