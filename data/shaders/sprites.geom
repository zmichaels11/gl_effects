#version 450 core

layout (points) in;
layout (triangle_strip, max_vertices = 4) out;

layout (location = 0)   in vec2 gUpperLeft[];
layout (location = 1)   in vec2 gUpperRight[];
layout (location = 2)   in vec2 gLowerLeft[];
layout (location = 3)   in vec2 gLowerRight[];
layout (location = 4)   in vec3 gTexCoord[];
layout (location = 5)   in int gColorIndex[];

layout (location = 0)   out vec3 fTexCoord;
layout (location = 1)   flat out mat3 fColorTransform;
layout (location = 4)   flat out vec4 fColorScale;

layout (binding = 0)    uniform samplerBuffer uColorTransform[4];

layout (binding = 0, std140)    uniform Matrices {
    mat4 uProjection;
};

void main() {
    vec3 c0 = texelFetch(uColorTransform[0], gColorIndex[0]).rgb;
    vec3 c1 = texelFetch(uColorTransform[1], gColorIndex[0]).rgb;
    vec3 c2 = texelFetch(uColorTransform[2], gColorIndex[0]).rgb;
    vec4 c3 = texelFetch(uColorTransform[3], gColorIndex[0]);

    fColorTransform = mat3(c0, c1, c2);
    fColorScale = c3;

    fTexCoord = vec3(0.0, 0.0, gTexCoord[0].z);
    gl_Position = uProjection * vec4(gUpperLeft[0], 0.0, 1.0);
    EmitVertex();

    fTexCoord = vec3(0.0, gTexCoord[0].yz);
    gl_Position = uProjection * vec4(gLowerLeft[0], 0.0, 1.0);
    EmitVertex();

    fTexCoord = vec3(gTexCoord[0].x, 0.0, gTexCoord[0].z);
    gl_Position = uProjection * vec4(gUpperRight[0], 0.0, 1.0);
    EmitVertex();

    fTexCoord = gTexCoord[0];
    gl_Position = uProjection * vec4(gLowerRight[0], 0.0, 1.0);
    EmitVertex();

    EndPrimitive();
}