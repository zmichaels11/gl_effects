#version 450 core

layout (location = 0)   in vec3 fTexCoord;
layout (location = 1)   flat in mat3 fColorTransform;
layout (location = 4)   flat in vec4 fColorScale;

layout (location = 0)   out vec4 result;

layout (binding = 4)    uniform sampler2DArray uSpritesheet;

void main() {
    vec4 color = texture(uSpritesheet, fTexCoord);
    
    result = vec4(fColorTransform * vec4(color.rgb, 1.0).rgb, color.a);    
    result *= fColorScale;
    result.rgb *= result.a;
}