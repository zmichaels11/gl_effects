#version 450 core

layout (location = 0)   in vec3 vPosition;
layout (location = 1)   in vec4 vColor;
layout (location = 2)   in mat4 vModelView;

layout (location = 0)   out vec4 fColor;

layout (binding = 0, std140)    uniform Matrices {
    mat4 uProjection;
};

void main() {
    fColor = vColor;
    gl_Position = uProjection * vModelView * vec4(vPosition, 1.0);
}