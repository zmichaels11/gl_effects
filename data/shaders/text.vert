#version 450 core

layout (location = 0)   in vec2 vPosition;
layout (location = 1)   in vec2 vTexCoord;
layout (location = 2)   in vec4 vColor;

layout (location = 0)   out vec2 fTexCoord;
layout (location = 1)   out vec4 fColor;

layout (binding = 0, std140)    uniform Matrices {
    mat4 uProjection;
};

void main() {
    gl_Position = uProjection * vec4(vPosition, 0.0, 1.0);
    fTexCoord = vTexCoord;
    fColor = vColor;
}